import {
  StyleSheet,
  View,
  FlatList,
  Image,
  ScrollView,
  AsyncStorage,
  Text,
  TouchableOpacity,
  Linking,
  Platform
} from "react-native";
import React, { Component } from "react";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
import Appbar from "../../components/AppbarTitle";
import ImageInformation from "../../Images/information.png";
import { RFPercentage, RFValue }  from "react-native-responsive-fontsize";
import WaterMark from "../../components/WatermarkIconBlack";
const Icon = createIconSetFromFontello(fontelloConfig);
import PropTypes from "prop-types";

export default class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          id: 0,
          title: "سام سیر",
          explane:
            " آژانس هواپیمایی سام سیر با مالکیت خصوصی در سال 1383 (2004) در تهران تاسیس گردید. این آژانس دارای مجوز بند «الف» از سازمان هواپیمایی کشوری و دارای مجوز بند «ب» از سازمان گردشگری و جهانگردی و عضو سازمان بین المللی حمل و نقل هوایی (یاتا) و مجهز به شبکه بین المللی آمادئوس و گابریل (سیستم رزرواسیون در شبکه حمل و نقل هوایی و بین المللی) و جزء 10 آژانس برتر ایران از لحاظ فروش می باشد  بخش عمده ای از فعالیت های این آژانس به شرح زیر است: 1- تهیه بلیط های داخلی و خارجی از کلیه ایرلاین ها 2- برگزار کننده مستقیم تورهای داخلی و خارجی3- رزرو هتل و اخذ ویزا4- تهیه بلیط های مسافرتی قطار5- تهیه بلیط های فوری و ضروری داخلی و خارجی6- ایجاد پکیج خصوصی انواع تور بنا به درخواست مسافران  از بدو تاسیس این آژانس خدمات مسافرتی، همیشه تلاش بر این بوده است که خدمات ارائه شده با بالاترین سطح کیفیت ممکن، جامع و متنوع مطابق با نیاز روز جهانی در صنعت گردشگری به مسافران عزیز، ارائه شود  در عصر ارتباطاتی که ما در حال حاضر، در آن حضور داریم و به دلیل سرعت بالای زندگی امروزه و نیاز انسان ها به خدماتی با سطح بالا و سرعت انجام بیشتر، این آژانس برای جلب رضایت مشتریان خود کوشیده است که سامانه خرید اینترنتی بلیط هواپیما داخلی و خارجی، هتل در سراسر نقاط دنیا، بیمه، اجاره یا رنت ماشین در هر کشوری از دنیا، ترانسفر و بسیاری از خدمات گردشگری را به صورت آنلاین و پشتیبانی شبانه روزی در اختیار مسافران قرار بدهد که در هر ساعتی از شبانه روز قادر به خرید بلیط های پرواز داخلی و خارجی و هتل های خود، بدون نیاز به حضور در محل آژانس باشند. آژانس هواپیمایی سام سیر تخصصی حرفه ای در زمینه انواع سفرهای سیاحتی (گروهی یا انفرادی) به آفریقا، استرالیا، اروپا، آسیای جنوب شرقی، هند، آمریکای لاتین و اخذ ویزا دارد و همچنین در زمینه برگزاری تورهای بین المللی و تخصصی نمایشگاهی جزء یکی از آژانس های برتر و پیشرو می باشد. یکی از نقاط قوت این آژانس، داشتن کادری مجرب و حرفه ای در زمینه مشاوره، فروش انواع بلیط های هواپیمایی و قطاری چه در داخل ایران و اروپا و یا سایر کشورهای جهان، رزرواسیون، پشتیبانی و همینطور تور لیدرهای مجرب در تمامی مسیرهای تورهای گروهی می باشد. شرکت خدمات مسافرت هوایی سام سیر، به بهره گیری از کارشناسان حرفه ای و همکاری کارگزاران قابل اعتماد و مجرب، توانایی اقدام و دریافت ویزای مسافرتی به اکثر کشورهای دنیا را دارد. همچنین ما برای آن دسته از توریست ها و علاقه مندانی که خواهان اکتشاف ایران، این کشور پهناور و کهن می باشند، خدمات ویژه ای در نظر گرفته ایم. پس از همین امروز با سام سیر همراه باشید! "
        },
        {
          id: 1,
          title: "خدمات سام سیر",
          explane:
            "صدور بلیط ایرلاین های داخلی و خارجی و بلیط قطـار  خدمات اخذ ویزا  برگزاری چارتر ایرلاین های داخلی و خارجی  صدور بیمه نامه مسافرتی داخـلی و خارجی  رزرواسیون هتل در سراسر ایران و جهان به صورت آنلاین و گارانتی شده با امتیاز انحصاری  ارائه خدمات VIP,CIP  برگزاری و اجرای تورهای نمایشگاهی تخصصی داخلی و خارجی  مشاوره امور مسافرتی حتی در روزهای تعطیل تورهای ورودی و اخذ ویزا"
        },
        {
          id: 2,
          title: "گواهینامه ها",
          explane: "یاتا"
        }
      ],
      selectedItem: 0
    };
  }
  navigateToBack() {
    this.props.navigation.navigate("FlightOptions");
  }
  _renderItem = ({ item }) => (
    <TouchableOpacity
      style={{ paddingRight:'7%' }}
      onPress={() => this.setState({ selectedItem: item.id })}
    >
      <Text
        style={[
          styles.titleText,
          { color: this.state.selectedItem == item.id ? "#11397F" : "#8A8795" }
        ]}
      >
        {item.title}
      </Text>
    </TouchableOpacity>
  );

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Appbar
          pageTitle="درباره سام سیر"
          backIconColor="#11397F"
          titleTextColor="#11397F"
          navigateTo={this.navigateToBack.bind(this)}
        />
        <View>
          <FlatList
            // style={{ alignSelf : 'center' , justifyContent : 'center'}}
            contentContainerStyle={styles.contentContainer}
            data={this.state.data}
            renderItem={this._renderItem}
            horizontal
            extraData={this.state}
          />
        </View>

        <View
          style={{
            borderRadius: 20,
            backgroundColor: "#9EADC560",
            height: "70%",
            marginStart: "7%",
            marginEnd: "7%",
            marginTop: 26
          }}
        >
          <ScrollView
            contentContainerStyle={styles.scrollContentContainer}
            style={{}}
          >
            <Text style={styles.infotext}>
              {this.state.data[this.state.selectedItem].explane}
            </Text>
          </ScrollView>
        </View>
        <WaterMark />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  contentContainer: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: "#9EADC5",
    flexDirection: "row-reverse",
    width: "100%",
    height: 33,  // check kon ba height passInfo mosavi bashe
    alignItems : 'center',
  },
  titleText: {
    fontSize: RFPercentage(2),
    fontFamily: "Sahel"
  },
  scrollContentContainer: {
    padding: 15,
    borderRadius: 20
  },
  infotext: {
    color: "#11397F",
    fontFamily: "Sahel",
    fontSize: RFPercentage(2.1),
    textAlign: "right"
  }
});
