import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Keyboard,
  ToastAndroid,
  Platform,
  StatusBar,
  Animated,
  Easing,
  Modal,
  Picker,
  AsyncStorage,
  TouchableWithoutFeedback
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
import PassengersCountMenu from "../components/PassengersCountMenu";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { PIXEL_RATIO } from "../constants/DeviceParameters";
import DatePicker from "react-native-date-picker";
import AppbarTitle from "../components/AppbarTitle";
import { connect } from "react-redux";
import { baseUrl } from "../constants/BaseURL";
import { SearchBar } from "react-native-elements";
import Fuse from "fuse.js";
import { USER_ID, REQUEST_ID, REQUEST_INDEX } from "../constants/UserParams";
import { Overlay, overlayBackgroundColor } from "react-native-elements";
import IconAnt from "react-native-vector-icons/AntDesign";
const axios = require("axios");
const Icon = createIconSetFromFontello(fontelloConfig);
const options = {
  keys: ["Text"]
};
class PassengersInformation extends Component {
  spinValue = new Animated.Value(0);
  spinValueFemale = new Animated.Value(0);
  constructor(props) {
    super(props);
    this.state = {
      // title: this.props.navigation.state.params.title,
      date: new Date(),
      modalVisible: false,
      dateModalVisible: false,
      birthDateModalVisible: false,
      passengerModalVisiblity: false,
      maleBtnColor: "white",
      femaleBtnColor: "white",
      maleIconColor: "darkgrey",
      femaleIconColor: "darkgrey",
      maleFontColor: "darkgrey",
      femaleFontColor: "darkgrey",
      Firstname: "",
      Lastname: "",
      PlaceOfBirth: "Place of Birth",
      PassportOriginality: "Passport Originality",
      NationalCode: "",
      PassportNumber: "",
      PassportExpiryDate: "",
      Birthday: "",
      expireYear: "",
      expireMonth: "",
      expireDay: "",
      birthYear: "",
      birthMonth: "",
      birthDay: "",
      Gender: "",
      language: "default",
      birthPlaceTextColor: "darkgrey",
      originalityTextColor: "darkgrey",
      areaClickIndicator: "",
      bwFirstName: 0,
      bwLastName: 0,
      bwPassNumber: 0,
      bwPassOrigin: 0,
      bwBirthPlace: 0,
      bwExpireDate: 0,
      bwBirthDate: 0,
      bwNationalCode: 0,
      // PassengersCountMenu
      array: [],
      number: 0,
      itemFontColor: "#333",
      itemPressed: 0,
      adults: 0,
      childs: 0,
      infants: 0,
      passengerType: "بزرگسال",
      passengerCollections: [],
      passenger_collection: [],
      nextPassenger: "مسافر بعدی",
      selectedIndex: 0,
      passengerModalVisible: false,
      //country params:
      countryList: [],
      mainCountryList: [],
      search: "",
      //companions
      companions: [],
      //available params:
      reqId: "",
      reqIndex: "",
      userId: "",
      flightDate: null,
      maxDate: new Date("2018-04-20"),
      minDate: new Date("2015-04-20"),
      isInternational: true
    };
  }

  selectedPassanger(value) {
    this.setState({
      itemPressed: value
    });
  }

  expireMinDate() {
    flightDate = new Date("2019-08-01");
    let expireDate = new Date(flightDate.setMonth(flightDate.getMonth() + 6));
    return expireDate;
  }

  birthMinDate(type) {
    let flightDate = new Date("2019-08-01");
    let minDate;
    console.log(type);
    if (type == "Adult") {
      // chech it with CTO. just see it from site :)
      minDate == new Date("1899-01-01");
      return minDate;
    } else if (type == "Child") {
      minDate = new Date(flightDate.setYear(flightDate.getFullYear() - 12));
      return minDate;
    } else if ((type = "Infant")) {
      minDate = new Date(flightDate.setYear(flightDate.getFullYear() - 2));
      return minDate;
    } else return;
  }

  birthMaxDate(type) {
    let flightDate = new Date("2019-08-01");
    let maxDate;
    console.log(type);
    if (type == "Adult") {
      // chech it with CTO. just see it from site :)
      maxDate = new Date(flightDate.setYear(flightDate.getFullYear() - 12));
      return maxDate;
    } else if (type == "Child") {
      maxDate = new Date(flightDate.setYear(flightDate.getFullYear() - 2));
      return maxDate;
    } else if (type == "Infant") {
      // maxDate = new Date(flightDate.setYear(flightDate.getFullYear()));
      // return maxDate;
      return flightDate;
    } else return;
  }

  setContents(ind) {
    this.setState({
      selectedIndex: ind,
      Firstname: this.state.passengerCollections[ind].Firstname,
      Lastname: this.state.passengerCollections[ind].Lastname,
      PlaceOfBirth: this.state.passengerCollections[ind].PlaceOfBirth,
      PassportOriginality: this.state.passengerCollections[ind]
        .PassportOriginality,
      PassportNumber: this.state.passengerCollections[ind].PassportNumber,
      NationalCode: this.state.passengerCollections[ind].NationalCode,
      birthPlaceTextColor: "black",
      originalityTextColor: "black",
      PassportExpiryDate: this.state.passengerCollections[ind]
        .PassportExpiryDate,
      Birthday: this.state.passengerCollections[ind].Birthday,
      expireYear: this.state.passengerCollections[ind].PassportExpiryDate.split(
        "-"
      )[0],
      expireMonth: this.state.passengerCollections[
        ind
      ].PassportExpiryDate.split("-")[1],
      expireDay: this.state.passengerCollections[ind].PassportExpiryDate.split(
        "-"
      )[2],
      birthDay: this.state.passengerCollections[ind].Birthday.split("-")[2],
      birthMonth: this.state.passengerCollections[ind].Birthday.split("-")[1],
      birthYear: this.state.passengerCollections[ind].Birthday.split("-")[0],
      bwFirstName: 0,
      bwLastName: 0,
      bwPassNumber: 0,
      bwPassOrigin: 0,
      bwBirthPlace: 0,
      bwExpireDate: 0,
      bwBirthDate: 0,
      bwNationalCode: 0,
      nextPassenger: "ذخیره تغییرات"
    });
    this.state.passengerCollections[ind].Gender == "Male"
      ? this.onMaleToggle()
      : this.state.passengerCollections[ind].Gender == "Female"
      ? this.onFemaleToggle()
      : null;
  }

  editPassenger() {
    if (this.state.Firstname == "") {
      this.setState({ bwFirstName: 2 });
    } else if (this.state.Lastname == "") {
      this.setState({ bwLastName: 2 });
    } else if (this.state.PassportNumber == "" && this.state.isInternational) {
      this.setState({ bwPassNumber: 2 });
    } else if (
      this.state.PassportOriginality == "Passport Originality" &&
      this.state.isInternational
    ) {
      this.setState({ bwPassOrigin: 2 });
    } else if (this.state.PlaceOfBirth == "Place of Birth") {
      this.setState({ bwBirthPlace: 2 });
    } else if (this.state.PassportExpiryDate == "") {
      this.setState({ bwExpireDate: 2 });
    } else if (this.state.Birthday == "") {
      this.setState({ bwBirthDate: 2 });
    } else if (this.state.NationalCode == "" && !this.state.isInternational) {
      this.setState({ bwNationalCode: 2 });
    } else if (this.state.Gender == "") {
      console.warn("setGender");
    } else if (
      this.state.bwFirstName != 2 &&
      this.state.bwLastName != 2 &&
      this.state.bwPassNumber != 2 &&
      this.state.bwPassOrigin != 2 &&
      this.state.bwBirthPlace != 2 &&
      this.state.bwExpireDate != 2 &&
      this.state.bwBirthDate != 2 &&
      this.state.bwNationalCode != 2
    ) {
      const newPassCollection = [...this.state.passengerCollections];
      newPassCollection[
        this.state.selectedIndex
      ].Firstname = this.state.Firstname;
      newPassCollection[
        this.state.selectedIndex
      ].Lastname = this.state.Lastname;
      newPassCollection[
        this.state.selectedIndex
      ].PassportNumber = this.state.PassportNumber;
      newPassCollection[
        this.state.selectedIndex
      ].NationalCode = this.state.NationalCode;
      newPassCollection[
        this.state.selectedIndex
      ].PassportOriginality = this.state.PassportOriginality;
      newPassCollection[
        this.state.selectedIndex
      ].PlaceOfBirth = this.state.PlaceOfBirth;
      newPassCollection[this.state.selectedIndex].PassportExpiryDate =
        this.state.expireYear +
        "-" +
        this.state.expireMonth +
        "-" +
        this.state.expireDay;
      newPassCollection[this.state.selectedIndex].Birthday =
        this.state.birthYear +
        "-" +
        this.state.birthMonth +
        "-" +
        this.state.birthDay;
      newPassCollection[this.state.selectedIndex].Gender = this.state.Gender;
      this.setState({
        passengerCollections: newPassCollection
      });
    }
  }

  async componentWillMount() {
    try {
      const userId = await AsyncStorage.getItem(USER_ID);
      const reqId = await AsyncStorage.getItem(REQUEST_ID);
      const reqIndex = await AsyncStorage.getItem(REQUEST_INDEX);
      await this.setState({ userId: userId, reqId: reqId, reqIndex: reqIndex });
      await this.getPrimaryInfo();
    } catch (error) {
      console.warn("userId is not available");
    }
  }

  UNSAFE_componentWillReceiveProps(props) {
    // this.setState({ adults: props.adultsNumber.toString() });
  }

  componentDidMount() {
    StatusBar.setHidden(false);
  }

  calculateDates() {
    let CurrentDate = new Date(this.state.flightDate);
    console.log(CurrentDate.getMonth());
    CurrentDate.setMonth(CurrentDate.getMonth() + 0);
    console.log(
      "Date after " + this.state.flightDate + " months:",
      CurrentDate
    );
    var str =
      CurrentDate.getDay() +
      "-" +
      CurrentDate.getMonth() +
      "-" +
      CurrentDate.getFullYear();
    console.log(str);
  }

  getPrimaryInfo() {
    var self = this;
    // ToDo : getting Req_ID and User_ID
    axios
      .post(baseUrl + "/Passenger/Information", {
        UserID: self.state.userId,
        Req_ID: self.state.reqId,
        Index: self.state.reqIndex

        //   UserID: "2",
        // Req_ID: "5d1886d9bad2775b58ac75c4",
        // Index: "0",
      })
      .then(function(response) {
        console.log(response);
        self.setState({
          countryList: response.data.Result.Country,
          mainCountryList: response.data.Result.Country,
          passenger_collection: response.data.Result.Passengers,
          flightDate: "2022-03-18",
          isInternational: response.data.Result.IsInternational
          // adults: response.data.Result.Adult,
          // childs: response.data.Result.Child,
          // infants: response.data.Result.Infant,
          // number:
          // response.data.Result.Adult +
          // response.data.Result.Child +
          // response.data.Result.Infant
        });
        self.calculateDates();
        // console.warn(response.data.Result.Country[1].Text.toString());
      })
      .catch(function(error) {
        console.warn(error);
      });
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  setDateModalVisible(visible) {
    this.setState({ dateModalVisible: visible });
  }

  setBirthDateModalVisible(visible) {
    this.setState({ birthDateModalVisible: visible });
  }

  setPassengerModalVisible(visible) {
    this.setState({ passengerModalVisiblity: visible });
  }

  spin() {
    this.spinValue.setValue(0);
    Animated.timing(this.spinValue, {
      toValue: 1,
      duration: 500,
      easing: Easing.linear
    }).start(this.spinValueFemale.setValue(0));
  }

  spinFemale() {
    this.spinValueFemale.setValue(0);
    Animated.timing(this.spinValueFemale, {
      toValue: 1,
      duration: 500,
      easing: Easing.linear
    }).start(this.spinValue.setValue(0));
  }

  onMaleToggle = () => {
    switch (this.state.maleBtnColor) {
      case "white":
        this.resetFemaleBtn();
        this.setState({ maleBtnColor: "#0D3477" });
        this.setState({ maleIconColor: "white" });
        this.setState({ maleFontColor: "white" });
        this.setState({ Gender: "Male" });
        break;
      default:
        this.setState({ maleBtnColor: "#0D3477" });
    }
    this.spin();
  };

  onFemaleToggle = () => {
    switch (this.state.femaleBtnColor) {
      case "white":
        this.resetMaleBtn();
        this.setState({ femaleBtnColor: "#0D3477" });
        this.setState({ femaleIconColor: "white" });
        this.setState({ femaleFontColor: "white" });
        this.setState({ Gender: "Female" });
        break;
      default:
        this.setState({ femaleBtnColor: "#0D3477" });
    }
    this.spinFemale();
  };

  resetMaleBtn = () => {
    this.setState({ maleBtnColor: "white" });
    this.setState({ maleIconColor: "darkgrey" });
    this.setState({ maleFontColor: "darkgrey" });
  };

  resetFemaleBtn = () => {
    this.setState({ femaleBtnColor: "white" });
    this.setState({ femaleIconColor: "darkgrey" });
    this.setState({ femaleFontColor: "darkgrey" });
  };

  updateValue(text, field) {
    switch (field) {
      case "Firstname":
        this.setState({ Firstname: text });
        break;
      case "Lastname":
        this.setState({ Lastname: text });
        break;
      case "PlaceOfBirth":
        this.setState({ PlaceOfBirth: text });
        break;
      case "PassportOriginality":
        this.setState({ PassportOriginality: text });
        break;
      case "PassportNumber":
        this.setState({ PassportNumber: text });
        break;
      case "PassportExpiryDate":
        this.setState({ PassportExpiryDate: text });
        break;
      case "Birthday":
        this.setState({ Birthday: text });
        break;
      case "Gender":
        this.setState({ Gender: text });
        break;
    }
  }

  async onNextStep(payload) {
    await this.checkInputs();
    if (this.state.itemPressed == this.state.passenger_collection.length) {
      await this.setState({ nextPassenger: "ثبت و ارسال" });
      this.sendPassengerInformation(payload);
    }
  }

  checkInputs() {
    if (this.state.Firstname == "") {
      this.setState({ bwFirstName: 2 });
    } else if (this.state.Lastname == "") {
      this.setState({ bwLastName: 2 });
    } else if (this.state.PassportNumber == "" && this.state.isInternational) {
      this.setState({ bwPassNumber: 2 });
    } else if (
      this.state.PassportOriginality == "Passport Originality" &&
      this.state.isInternational
    ) {
      this.setState({ bwPassOrigin: 2 });
    } else if (this.state.PlaceOfBirth == "Place of Birth") {
      this.setState({ bwBirthPlace: 2 });
    } else if (this.state.NationalCode == "" && !this.state.isInternational) {
      this.setState({ bwNationalCode: 2 });
    } else if (
      this.state.PassportExpiryDate == "" &&
      this.state.isInternational
    ) {
      this.setState({ bwExpireDate: 2 });
    } else if (this.state.Birthday == "") {
      this.setState({ bwBirthDate: 2 });
    } else if (this.state.Gender == "") {
      this.spinValue.setValue(0);
      Animated.timing(this.spinValue, {
        toValue: 1,
        duration: 200,
        easing: Easing.linear
      }).start();

      this.spinValueFemale.setValue(0);
      Animated.timing(this.spinValueFemale, {
        toValue: 1,
        duration: 200,
        easing: Easing.linear
      }).start();
    } else if (
      this.state.bwFirstName != 2 &&
      this.state.bwLastName != 2 &&
      this.state.bwPassNumber != 2 &&
      this.state.bwPassOrigin != 2 &&
      this.state.bwBirthPlace != 2 &&
      this.state.bwExpireDate != 2 &&
      this.state.bwBirthDate != 2 &&
      this.state.bwNationalCode != 2
    ) {
      const collection = {};
      collection.Firstname = this.state.Firstname;
      collection.Lastname = this.state.Lastname;
      collection.PassportNumber = this.state.PassportNumber;
      collection.NationalCode = this.state.NationalCode;
      collection.PassportOriginality = this.state.PassportOriginality;
      collection.PlaceOfBirth = this.state.PlaceOfBirth;
      collection.PassportExpiryDate =
        this.state.expireYear +
        "-" +
        this.state.expireMonth +
        "-" +
        this.state.expireDay;
      collection.Birthday =
        this.state.birthYear +
        "-" +
        this.state.birthMonth +
        "-" +
        this.state.birthDay;
      collection.Gender = this.state.Gender;

      
      // if (this.state.NationalCode.length == 10) {
      //   if (
      //     this.state.NationalCode == "1111111111" ||
      //     this.state.NationalCode == "0000000000" ||
      //     this.state.NationalCode == "2222222222" ||
      //     this.state.NationalCode == "3333333333" ||
      //     this.state.NationalCode == "4444444444" ||
      //     this.state.NationalCode == "5555555555" ||
      //     this.state.NationalCode == "6666666666" ||
      //     this.state.NationalCode == "7777777777" ||
      //     this.state.NationalCode == "8888888888" ||
      //     this.state.NationalCode == "9999999999"
      //   ) {
      //     // return false;
      //   this.setState({ bwNationalCode: 2 });
      //   }
      //   c = parseInt(this.state.NationalCode.charAt(9));
      //   n =
      //     parseInt(this.state.NationalCode.charAt(0)) * 10 +
      //     parseInt(this.state.NationalCode.charAt(1)) * 9 +
      //     parseInt(this.state.NationalCode.charAt(2)) * 8 +
      //     parseInt(this.state.NationalCode.charAt(3)) * 7 +
      //     parseInt(this.state.NationalCode.charAt(4)) * 6 +
      //     parseInt(this.state.NationalCode.charAt(5)) * 5 +
      //     parseInt(this.state.NationalCode.charAt(6)) * 4 +
      //     parseInt(this.state.NationalCode.charAt(7)) * 3 +
      //     parseInt(this.state.NationalCode.charAt(8)) * 2;
      //   r = n - parseInt(n / 11) * 11;
      //   if ((r == 0 && r == c) || (r == 1 && c == 1) || (r > 1 && c == 11 - r)) {
      //     // return true;
      //     this.selectedPassanger(this.state.itemPressed + 1);
      //     this.clearFields();
      //     this.state.passengerCollections.push(collection);
      //     this.setState({ passengerCollections: this.state.passengerCollections });
      //   } else {
      //     // return false;
      //   this.setState({ bwNationalCode: 2 });
      //   }
      // } else {
      //   // return false;
      //   this.setState({ bwNationalCode: 2 });
      // }
      
      this.selectedPassanger(this.state.itemPressed + 1);
      this.clearFields();
      this.state.passengerCollections.push(collection);
      this.setState({ passengerCollections: this.state.passengerCollections });
    }
  }

  async sendPassengerInformation(payload) {
    if (this.state.nextPassenger != "ثبت و ارسال") {
      await this.onNextStep();
    }
    await this.getPaymentPrimaryInfo(payload);
  }

  getPaymentPrimaryInfo(payload) {
    var self = this;
    axios
      .post(baseUrl + "/flight/payment", {
        UserID: self.state.userId,
        Req_ID: self.state.reqId,
        Index: self.state.reqIndex
      })
      .then(function(response) {
        self.props.navigation.navigate("Payment", {
          passengerList: payload.Passengers,
          paymentPrimaryInfo: response.data.Result,
          reqIndex: self.state.reqIndex
        });
        console.log(response);
      })
      .catch(function(error) {
        console.warn(error);
      });
  }

  clearFields = () => {
    this.setState({
      Firstname: "",
      Lastname: "",
      PassportNumber: "",
      NationalCode: "",
      PlaceOfBirth: "Place of Birth",
      PassportOriginality: "Passport Originality",
      expireYear: "",
      expireMonth: "",
      expireDay: "",
      PassportExpiryDate: "",
      birthYear: "",
      birthMonth: "",
      birthDay: "",
      Birthday: "",
      Gender: "",
      birthPlaceTextColor: "darkgrey",
      originalityTextColor: "darkgrey"
    });
    this.resetMaleBtn();
    this.resetFemaleBtn();
  };

  showPassModal(passengerModalVisible) {
    this.setState({
      passengerModalVisible: passengerModalVisible
    });
  }

  navigateToBack() {
    this.props.navigation.navigate("FlightOptions");
  }

  async updateSearch(text) {
    await this.setState({ countryList: this.state.mainCountryList });
    const fuse = new Fuse(this.state.countryList, options);
    this.setState({ countryList: fuse.search(text) });
    if (text.length == 0) {
      this.setState({ countryList: this.state.mainCountryList });
    }
  }

  goIndex = () => {
    this.flatListRef.scrollToIndex({ animated: true, index: 6 });
  };

  validateNationalCode(text) {

    if (this.state.NationalCode.length == 10) {
      if (
        this.state.NationalCode == "1111111111" ||
        this.state.NationalCode == "0000000000" ||
        this.state.NationalCode == "2222222222" ||
        this.state.NationalCode == "3333333333" ||
        this.state.NationalCode == "4444444444" ||
        this.state.NationalCode == "5555555555" ||
        this.state.NationalCode == "6666666666" ||
        this.state.NationalCode == "7777777777" ||
        this.state.NationalCode == "8888888888" ||
        this.state.NationalCode == "9999999999"
      ) {
        // return false;
      this.setState({ bwNationalCode: 2 });
      }
      c = parseInt(this.state.NationalCode.charAt(9));
      n =
        parseInt(this.state.NationalCode.charAt(0)) * 10 +
        parseInt(this.state.NationalCode.charAt(1)) * 9 +
        parseInt(this.state.NationalCode.charAt(2)) * 8 +
        parseInt(this.state.NationalCode.charAt(3)) * 7 +
        parseInt(this.state.NationalCode.charAt(4)) * 6 +
        parseInt(this.state.NationalCode.charAt(5)) * 5 +
        parseInt(this.state.NationalCode.charAt(6)) * 4 +
        parseInt(this.state.NationalCode.charAt(7)) * 3 +
        parseInt(this.state.NationalCode.charAt(8)) * 2;
      r = n - parseInt(n / 11) * 11;
      if ((r == 0 && r == c) || (r == 1 && c == 1) || (r > 1 && c == 11 - r)) {
        // return true;
        this.selectedPassanger(this.state.itemPressed + 1);
        this.clearFields();
        this.state.passengerCollections.push(collection);
        this.setState({ passengerCollections: this.state.passengerCollections });
      } else {
        // return false;
      this.setState({ bwNationalCode: 2 });
      }
    } else {
      // return false;
      this.setState({ bwNationalCode: 2 });
    }

    this.setState({ NationalCode: text.trim(), bwNationalCode: 0 });
  }

  validatePassportNumber(text) {
    // let passportReg = /^(?!^0+$)[a-zA-Z0-9]{8,9}$/;
    let passportReg = /^[A-PR-WY][1-9]\d\s?\d{4,5}[1-9]$/gi;
    if (passportReg.test(text) == true) {
      this.updateValue(text, "PassportNumber");
      this.setState({ bwPassNumber: 0 });
    } else {
      this.updateValue(text, "PassportNumber");
      this.setState({ bwPassNumber: 2 });
      return false;
    }
  }

  validateFirstName(text) {
    let englishReg = /^[a-zA-Z ]+$/;
    if (englishReg.test(text) && !text.startsWith(" ", 0)) {
      this.updateValue(text, "Firstname");
      this.setState({ bwFirstName: 0 });
    } else {
      this.updateValue(text, "Firstname");

      // this.updateValue(text, "Firstname");
      this.setState({ bwFirstName: 2 });
      return false;
    }
  }

  validateLastName(text) {
    let englishReg = /^[a-zA-Z ]+$/;
    if (englishReg.test(text)) {
      this.updateValue(text, "Lastname");
      this.setState({ bwLastName: 0 });
    } else {
      if (text == "") this.updateValue(text, "Lastname");
      this.setState({ bwLastName: 2 });
      return false;
    }
  }

  render() {
    let payload = {
      Passengers: this.state.passengerCollections,
      UserID: this.state.userId,
      Req_ID: this.state.reqId,
      Index: this.state.reqIndex,
      PayType: "0"
    };
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ["0deg", "360deg"]
    });
    const spinFemale = this.spinValueFemale.interpolate({
      inputRange: [0, 1],
      outputRange: ["0deg", "360deg"]
    });

    return (
      <LinearGradient
        colors={["#1B275A", "#1B275A", "#1B275A"]}
        style={{ flex: 1 }}
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
      >
        {/* Countries Modal */}
        <Modal
          animationType="slide"
          transparent={true}
          // supportedOrientations="portrait"
          visible={this.state.modalVisible}
          // presentationStyle="pageSheet"
          onRequestClose={() => {
            this.setModalVisible(!this.state.modalVisible);
            if (this.state.areaClickIndicator == "origin") {
              // this.setState({ originalityTextColor: "black" });
            } else if (this.state.areaClickIndicator == "birthPlace") {
              // this.setState({ birthPlaceTextColor: "black" });
            } else {
              this.setState({ birthPlaceTextColor: "darkgrey" });
              this.setState({ originalityTextColor: "darkgrey" });
            }
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "center"
            }}
          >
            <View
              style={{
                backgroundColor: "white",
                flex: 1,
                marginHorizontal: 0,
                padding: 0,
                borderRadius: 0
              }}
            >
              {/* https://react-native-training.github.io/react-native-elements/docs/searchbar.html#platform */}

              <SearchBar
                containerStyle={{
                  backgroundColor: "white",
                  paddingHorizontal: -4
                }}
                leftIconContainerStyle={{
                  paddingHorizontal: 0
                }}
                searchIcon={
                  <Icon
                    name="back"
                    style={{ padding: 12 }}
                    onPress={() =>
                      this.setState({
                        modalVisible: false
                      })
                    }
                  />
                }
                inputContainerStyle={{ backgroundColor: "white" }}
                placeholder="Search your country..."
                onChangeText={text => (
                  this.updateSearch(text), this.setState({ search: text })
                )}
                value={this.state.search}
              />

              {/* <View
                style={{ height: 2, width: "100%", backgroundColor: "#B5C9E8" }}
              /> */}
              <FlatList
                // style={PassengersInfoStyles.flatList}
                data={this.state.countryList}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.state.areaClickIndicator == "origin"
                        ? (this.setState({
                            modalVisible: false,
                            originalityTextColor: "black",
                            bwPassOrigin: 0
                          }),
                          this.updateValue(item.ID, "PassportOriginality"))
                        : this.state.areaClickIndicator == "birthPlace"
                        ? (this.setState({
                            modalVisible: false,
                            birthPlaceTextColor: "black",
                            bwBirthPlace: 0
                          }),
                          this.updateValue(item.ID, "PlaceOfBirth"))
                        : null
                    }
                  >
                    <View style={{ paddingHorizontal: 24 }}>
                      <Text
                        style={[PassengersInfoStyles.menuOptionText]}
                        // onPress={() => this.selectedPassanger(item.index)}
                      >
                        {item.Text}
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                )}
              />
            </View>
          </View>
        </Modal>

        {/* modal implementation of expire date picker: */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.dateModalVisible}
          onRequestClose={() => {
            this.setDateModalVisible(!this.state.dateModalVisible);

            // console.warn('date',this.state.gregDate.split('-'));
            // const [week, mm, dd, yyyy]= this.state.date.toString().split(" ")
            // this.setState({dateArray: [this.state.date.toString(), this.state.date.getMonth().toString(), this.state.date.getDate().toString(), this.state.date.getFullYear().toString()] })
            // const mm = this.state.date.getMonth()
            // console.warn(mm + dd + yyyy)
            // this.setState({ expireYear: this.state.date.getFullYear().toString() })
            // console.warn(this.state.expireYear)
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "flex-end"
            }}
          >
            <View
              style={{
                backgroundColor: "whitesmoke",
                borderRadius: 12,
                alignContent: "center",
                justifyContent: "center",
                position: "absolute",
                bottom: 0,
                right: 0,
                left: 0
              }}
            >
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  // borderTopRightRadius: 12,
                  // borderTopLeftRadius: 12,
                  borderRadius: 12,
                  padding: 8
                  // position: "absolute",
                  // bottom: 0,
                  // right: 16,
                  // left: 16,
                  // flex: 1,
                  // height: 30,
                  // backgroundColor: "whitesmoke"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    width: "100%"
                  }}
                >
                  {/* <Text
                    style={{ color: "#000", fontSize: RFPercentage(2.5), padding: 8 }}
                    onPress={() => {
                      this.setState({
                        expireYear: "",
                        expireMonth: "",
                        expireDay: ""
                      });
                      this.setDateModalVisible(!this.state.dateModalVisible);
                    }}
                  >
                    Cancel
                  </Text>
                  <Text
                    style={{ color: "#000", fontSize: RFPercentage(2.5), padding: 8 }}
                    onPress={() => {
                      this.setState({
                        expireYear: this.state.date.getFullYear().toString(),
                        expireMonth: (
                          this.state.date.getMonth() + 1
                        ).toString(),
                        expireDay: this.state.date.getDate().toString()
                      });
                      this.setDateModalVisible(!this.state.dateModalVisible);
                    }}
                  >
                    Ok
                  </Text> */}

                  <Icon
                    style={{
                      color: "#B5C9E8",
                      fontSize: RFPercentage(2.5),
                      padding: 8
                    }}
                    onPress={() => {
                      this.setState({
                        expireYear: "",
                        expireMonth: "",
                        expireDay: "",
                        PassportExpiryDate: ""
                      });
                      this.setDateModalVisible(!this.state.dateModalVisible);
                    }}
                    name="cancel"
                  />
                  <Icon
                    style={{
                      color: "#B5C9E8",
                      fontSize: RFPercentage(3.9),
                      padding: 8
                    }}
                    onPress={() => {
                      this.setState({
                        expireYear: this.state.date.getFullYear().toString(),
                        expireMonth: (
                          this.state.date.getMonth() + 1
                        ).toString(),
                        expireDay: this.state.date.getDate().toString(),
                        bwExpireDate: 0,
                        PassportExpiryDate:
                          this.state.date.getFullYear().toString() +
                          "-" +
                          (this.state.date.getMonth() + 1).toString() +
                          "-" +
                          this.state.date.getDate().toString()
                      });
                      this.setDateModalVisible(!this.state.dateModalVisible);
                    }}
                    name="checkmark"
                  />
                </View>
                {
                  // console.log('current index' , this.state.itemPressed , this.state.passenger_collection[this.state.itemPressed].Type)
                }
                <DatePicker
                  style={
                    {
                      // flexDirection: "row"
                      // backgroundColor: "#efefef",
                      // justifyContent: "center",
                      // alignItems: "center",
                      // backgroundColor: "orange",
                      // paddingBottom: 20,
                      // flex: 1
                    }
                  }
                  date={this.state.date}
                  mode={"date"}
                  fadeToColor={"none"}
                  // locale={"fa"}
                  minimumDate={this.expireMinDate()}
                  // maximumDate = {this.state.maxDate}
                  onDateChange={date => {
                    this.setState({ date: date });
                    // this.setState({
                    //   expireYear: date.getFullYear().toString()
                    // });
                  }}
                  // const [name, street, unit, city, state, zip] = date.split('-');
                />
              </View>
            </View>
          </View>
        </Modal>

        {/* modal implementation of birth date picker: */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.birthDateModalVisible}
          onRequestClose={() => {
            this.setBirthDateModalVisible(!this.state.birthDateModalVisible);
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "flex-end"
            }}
          >
            <View
              style={{
                backgroundColor: "whitesmoke",
                borderRadius: 12,
                alignContent: "center",
                justifyContent: "center",
                position: "absolute",
                bottom: 0,
                right: 0,
                left: 0
              }}
            >
              <View
                style={{
                  flexDirection: "column",
                  // justifyContent: "center",
                  alignItems: "center",
                  // borderTopRightRadius: 12,
                  // borderTopLeftRadius: 12,
                  borderRadius: 12,
                  padding: 8
                  // position: "absolute",
                  // bottom: 0,
                  // right: 16,
                  // left: 16,
                  // flex: 1,
                  // height: 30,
                  // backgroundColor: "whitesmoke"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    width: "100%"
                  }}
                >
                  <Icon
                    style={{
                      color: "#B5C9E8",
                      fontSize: RFPercentage(2.5),
                      padding: 8
                    }}
                    onPress={() => {
                      this.setState({
                        birthYear: "",
                        birthMonth: "",
                        birthDay: "",
                        Birthday: ""
                      });
                      this.setBirthDateModalVisible(
                        !this.state.birthDateModalVisible
                      );
                    }}
                    name="cancel"
                  />
                  <Icon
                    style={{
                      color: "#B5C9E8",
                      fontSize: RFPercentage(3.9),
                      padding: 8
                    }}
                    onPress={() => {
                      this.setState({
                        birthYear: this.state.date.getFullYear().toString(),
                        birthMonth: (this.state.date.getMonth() + 1).toString(),
                        birthDay: this.state.date.getDate().toString(),
                        bwBirthDate: 0,
                        Birthday:
                          this.state.date.getFullYear().toString() +
                          "-" +
                          (this.state.date.getMonth() + 1).toString() +
                          "-" +
                          this.state.date.getDate().toString()
                      });
                      this.setBirthDateModalVisible(
                        !this.state.birthDateModalVisible
                      );
                    }}
                    name="checkmark"
                  />
                </View>
                <DatePicker
                  style={
                    {
                      // flexDirection: "row"
                      // backgroundColor: "#efefef",
                      // justifyContent: "center",
                      // alignItems: "center",
                      // backgroundColor: "orange",
                      // paddingBottom: 20,
                      // flex: 1
                    }
                  }
                  date={this.state.date}
                  mode={"date"}
                  fadeToColor={"none"}
                  minimumDate={this.birthMinDate(
                    this.state.passenger_collection[this.state.itemPressed]
                      ? this.state.passenger_collection[this.state.itemPressed]
                          .Type
                      : null
                  )}
                  maximumDate={this.birthMaxDate(
                    this.state.passenger_collection[this.state.itemPressed]
                      ? this.state.passenger_collection[this.state.itemPressed]
                          .Type
                      : null
                  )}
                  onDateChange={date => {
                    this.setState({ date: date });
                    // this.setState({
                    //   expireYear: date.getFullYear().toString()
                    // });
                  }}
                />
              </View>
            </View>
          </View>
        </Modal>

        {/* companions modal */}
        <Modal
          animationType="slide"
          transparent={true}
          // supportedOrientations="portrait"
          visible={this.state.passengerModalVisible}
          // presentationStyle="pageSheet"
          onRequestClose={() => {
            this.setModalVisible(!this.state.passengerModalVisible);
            if (this.state.areaClickIndicator == "origin") {
              // this.setState({ originalityTextColor: "black" });
            } else if (this.state.areaClickIndicator == "birthPlace") {
              // this.setState({ birthPlaceTextColor: "black" });
            } else {
              this.setState({ birthPlaceTextColor: "darkgrey" });
              this.setState({ originalityTextColor: "darkgrey" });
            }
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "center"
            }}
          >
            <View
              style={{
                backgroundColor: "white",
                flex: 1,
                marginHorizontal: 0,
                padding: 0,
                borderRadius: 0
              }}
            >
              {/* https://react-native-training.github.io/react-native-elements/docs/searchbar.html#platform */}

              <SearchBar
                containerStyle={{
                  backgroundColor: "white",
                  paddingHorizontal: -4
                }}
                leftIconContainerStyle={{
                  paddingHorizontal: 0
                }}
                searchIcon={
                  <Icon
                    name="back"
                    style={{ padding: 12 }}
                    onPress={() =>
                      this.setState({
                        passengerModalVisible: false
                      })
                    }
                  />
                }
                inputContainerStyle={{ backgroundColor: "white" }}
                placeholder="Search your country..."
                onChangeText={text => (
                  this.updateSearch(text), this.setState({ search: text })
                )}
                value={this.state.search}
              />

              {/* <View
                style={{ height: 2, width: "100%", backgroundColor: "#B5C9E8" }}
              /> */}
              <FlatList
                // style={PassengersInfoStyles.flatList}
                data={this.state.countryList}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.state.areaClickIndicator == "origin"
                        ? (this.setState({
                            modalVisible: false,
                            originalityTextColor: "black",
                            bwPassOrigin: 0
                          }),
                          this.updateValue(item.Text, "PassportOriginality"))
                        : this.state.areaClickIndicator == "birthPlace"
                        ? (this.setState({
                            modalVisible: false,
                            birthPlaceTextColor: "black",
                            bwBirthPlace: 0
                          }),
                          this.updateValue(item.Text, "PlaceOfBirth"))
                        : null
                    }
                  >
                    <View style={{ paddingHorizontal: 24 }}>
                      <Text
                        style={[PassengersInfoStyles.menuOptionText]}
                        // onPress={() => this.selectedPassanger(item.index)}
                      >
                        {item.Text}
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                )}
              />
            </View>
          </View>
        </Modal>

        <Overlay
          isVisible={this.state.passengerModalVisiblity}
          windowBackgroundColor="rgba(0, 0, 0, .7)"
          overlayBackgroundColor="white"
          width="80%"
          height="70%"
          height="auto"
          borderRadius={6}
        >
          <View>
            <View>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ passengerModalVisiblity: false });
                }}
                style={{
                  padding: 10
                }}
              >
                <Icon name="cancel" color="#B5C9E8" size={15} />
              </TouchableOpacity>
              <View
                style={{
                  alignItems: "center",
                  position: "absolute",
                  alignSelf: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: RFPercentage(2.2),
                    fontFamily: "Sahel",
                    color: "#15479F"
                  }}
                >
                  همراهان من
                </Text>
              </View>
            </View>
            <View
              style={{
                backgroundColor: "#F9C476",
                marginTop: 10,
                alignItems: "flex-end",
                borderRadius: 6
              }}
            >
              <View style={{ flexDirection: "row-reverse" }}>
                <Icon
                  name={
                    this.state.passenger_collection[this.state.itemPressed]
                      ? this.state.passenger_collection[
                          this.state.itemPressed
                        ].Type.toLowerCase()
                      : "adult"
                  } //set default
                  color="#15479F"
                  size={20}
                  style={{ padding: 2, paddingRight: 13 }}
                />
                <Text
                  style={{
                    fontFamily: "Sahel",
                    color: "#15479F",
                    textAlign: "right",
                    padding: 2
                  }}
                >
                  {this.state.passenger_collection[this.state.itemPressed] !=
                  null
                    ? this.state.passenger_collection[this.state.itemPressed]
                        .Previous_Passengers.length
                    : null}{" "}
                  نتیجه
                </Text>
              </View>
            </View>
            {/* {console.log(this.state.passenger_collection[this.state.itemPressed].Type.toLowerCase(), 'pass')}  */}
            <FlatList
              data={
                this.state.passenger_collection[this.state.itemPressed] != null
                  ? this.state.passenger_collection[this.state.itemPressed]
                      .Previous_Passengers
                  : null
              }
              renderItem={({ item }) => (
                <View style={{}}>
                  <View
                    style={{
                      flexDirection: "row-reverse",
                      justifyContent: "space-between",
                      padding: 8
                    }}
                  >
                    <Icon
                      name={
                        this.state.passenger_collection[this.state.itemPressed]
                          ? this.state.passenger_collection[
                              this.state.itemPressed
                            ].Type.toLowerCase()
                          : "adult"
                      } //set default
                      color="#FFBA00"
                      size={20}
                      style={{ padding: 5 }}
                    />
                    <View>
                      <Text
                        style={{
                          color: "#8A8795",
                          fontSize: RFPercentage(1.8),
                          fontFamily: "Montserrat-Regular"
                        }}
                      >
                        Passport Number
                      </Text>
                      <Text
                        style={{
                          fontSize: RFPercentage(1.9),
                          fontFamily: "Montserrat-Regular",
                          color: "#11397F",
                          paddingTop: 5
                        }}
                      >
                        {item.PassportNumber}
                      </Text>
                    </View>
                    <View style={{ width: "40%" }}>
                      <Text
                        numberOfLines={2}
                        style={{
                          fontFamily: "Montserrat-Regular",
                          fontSize: RFPercentage(1.9),
                          color: "#11397F"
                        }}
                      >
                        {item.FirstName} {item.LastName}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      borderTopWidth: 1,
                      borderColor: "#8A8795",
                      width: "100%"
                    }}
                  />
                </View>
              )}
              style={{ height: 400, marginTop: 15 }}
            />
          </View>
        </Overlay>

        <StatusBar backgroundColor="#1B275A" />

        <AppbarTitle
          pageTitle="اطلاعات مسافران"
          backIconColor="white"
          titleTextColor="white"
          // menuIcon="edits"
          navigateTo={this.navigateToBack.bind(this)}
          // showPassengerModal={this.showPassModal.bind(this)}
        />

        <View style={{ flexDirection: "row-reverse" }}>
          <FlatList
            style={PassengersInfoStyles.flatList}
            inverted={true}
            horizontal={true}
            // data={this.state.array}
            data={this.state.passenger_collection}
            extraData={this.state}
            keyExtractor={(item, index) => index.toString()}
            // getItemLayout={(data, index) => {
            //   return { length: 33, index, offset: 33 * index };
            // }}
            ref={ref => {
              this.flatListRef = ref;
            }}
            const
            itemType=""
            const
            itemCounter=""
            ListFooterComponent={({ item }) => (
              <Text
                style={[
                  PassengersInfoStyles.menuOptionText,
                  {
                    color: "orange",
                    paddingStart: 24
                  }
                ]}
              >
                اطلاعات تماس
              </Text>
            )}
            renderItem={({ item }) => (
              item.Type == "Adult"
                ? (itemType = "بزرگسال")
                : item.Type == "Child"
                ? (itemType = "خردسال")
                : item.Type == "Infant"
                ? (itemType = "نوزاد")
                : "",
              item.Counter == 1
                ? (itemCounter = "اول")
                : item.Counter == 2
                ? (itemCounter = "دوم")
                : item.Counter == 3
                ? (itemCounter = "سوم")
                : item.Counter == 4
                ? (itemCounter = "چهارم")
                : item.Counter == 5
                ? (itemCounter = "پنجم")
                : item.Counter == 6
                ? (itemCounter = "ششم")
                : item.Counter == 7
                ? (itemCounter = "هفتم")
                : item.Counter == 8
                ? (itemCounter = "هشتم")
                : item.Counter == 9
                ? (itemCounter = "نهم")
                : "",
              (
                <View style={{ paddingHorizontal: 24 }}>
                  <Text
                    style={[
                      PassengersInfoStyles.menuOptionText,
                      {
                        color:
                          this.state.itemPressed === item.Index
                            ? "orange"
                            : "grey"
                      }
                    ]}
                    onPress={
                      () =>
                        item.Index < this.state.passengerCollections.length
                          ? (this.selectedPassanger(item.Index),
                            this.setContents(item.Index))
                          : item.Index ==
                              this.state.passengerCollections.length &&
                            item.Index <
                              this.state.passenger_collection.length - 1
                          ? (this.selectedPassanger(item.Index),
                            this.clearFields(),
                            this.setState({ nextPassenger: "مسافر بعدی" }))
                          : item.Index + 1 ==
                            this.state.passenger_collection.length
                          ? (this.setState({ nextPassenger: "ثبت و ارسال" }),
                            this.selectedPassanger(item.Index))
                          : null
                      // this.goIndex()
                    }
                  >
                    {itemType} {itemCounter}
                  </Text>
                </View>
              )
            )}
          />
        </View>

        <ScrollView style={{ zIndex: 1 }}>
          <View style={PassengersInfoStyles.mainContainer}>
            <View
              style={{
                marginBottom: 13,
                marginTop: 3,
                justifyContent: "space-between",
                flexDirection: "row"
              }}
            >
              {/* <Text
                style={{
                  fontFamily: "Sahel",
                  fontSize: RFPercentage(2.4),
                  color: "#fff",
                  includeFontPadding: false
                }}
                // onPress={() => {
                //   console.warn(
                //     this.state.passengerCollections.length.toString()
                //   );
                // }}
              >
                {this.state.passengerType}
              </Text> */}

              <TouchableWithoutFeedback
                onPress={() => {
                  // this.showPassModal(true);
                  // console.warn(
                  //   this.state.passengerCollections.toString()
                  // );
                  this.setState({
                    passengerModalVisiblity: true
                  });
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    textAlign: "center",
                    alignContent: "center",
                    alignSelf: "center",
                    borderRadius: 6,
                    padding: 3,
                    backgroundColor: "orange"
                  }}
                >
                  <View
                    style={{
                      marginEnd: 6,
                      justifyContent: "center"
                    }}
                  >
                    <IconAnt
                      name="star"
                      color="#1B275A"
                      style={{ fontSize: RFPercentage(3) }}
                    />
                  </View>
                  <Text
                    style={{
                      textAlign: "center",
                      fontFamily: "Sahel-Bold",
                      fontSize: RFPercentage(2.4),
                      color: "#1B275A",
                      includeFontPadding: false
                    }}
                  >
                    همراهان من
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            </View>

            <TextInput
              style={[
                PassengersInfoStyles.formInputText,
                {
                  borderWidth: this.state.bwFirstName,
                  height: Platform.OS === "ios" ? 46 : null
                }
              ]}
              placeholder={this.props.firstName}
              keyboardType="name-phone-pad"
              autoCapitalize="words"
              returnKeyType={"next"}
              value={this.state.Firstname}
              onSubmitEditing={() => {
                this.lastName.focus();
              }}
              blurOnSubmit={false}
              // onChangeText={text => (
              //   this.updateValue(text, "Firstname"),
              //   text.startsWith(" ")
              //     ? this.setState({ Firstname: text.replace(" ", "") })
              //     : this.setState({ bwFirstName: 0 })
              // )}
              onChangeText={text => this.validateFirstName(text)}
            />

            <TextInput
              style={[
                PassengersInfoStyles.formInputText,
                {
                  borderWidth: this.state.bwLastName,
                  height: Platform.OS === "ios" ? 46 : null
                }
              ]}
              placeholder={this.props.lastName}
              keyboardType="name-phone-pad"
              value={this.state.Lastname}
              autoCapitalize="words"
              ref={input => {
                this.lastName = input;
              }}
              returnKeyType={"next"}
              onSubmitEditing={() => {
                this.birthPlace.focus();
              }}
              blurOnSubmit={false}
              // onChangeText={text => (
              //   this.updateValue(text, "Lastname"),
              //   text.startsWith(" ")
              //     ? this.setState({ Lastname: text.replace(" ", "") })
              //     : this.setState({ bwLastName: 0 })
              // )}
              onChangeText={text => this.validateLastName(text)}
            />

            {this.state.isInternational ? (
              <TextInput
                style={[
                  PassengersInfoStyles.formInputText,
                  {
                    borderWidth: this.state.bwPassNumber,
                    height: Platform.OS === "ios" ? 46 : null
                  }
                ]}
                placeholder={this.props.passNumber}
                keyboardType="default"
                value={this.state.PassportNumber}
                autoCapitalize="words"
                ref={input => {
                  this.birthPlace = input;
                }}
                // onChangeText={text => (
                //   this.updateValue(text, "PassportNumber"),
                //   text.startsWith(" ")
                //     ? this.setState({ PassportNumber: text.replace(" ", "") })
                //     : this.setState({ bwPassNumber: 0 })
                // )}
                onChangeText={text => this.validatePassportNumber(text)}
              />
            ) : null}

            <View
              style={[
                PassengersInfoStyles.passportContainerStyle,
                {
                  flex: 1,
                  alignItems: "center",
                  marginBottom: 10
                }
              ]}
            >
              {this.state.isInternational ? (
                <View
                  style={[
                    PassengersInfoStyles.passportContainerStyle,
                    {
                      backgroundColor: "white",
                      alignItems: "center",
                      borderRadius: 6,
                      height: 46,
                      flex: 0.5,
                      borderColor: "red",
                      borderWidth: this.state.bwPassOrigin
                    }
                  ]}
                >
                  <Text
                    style={[
                      PassengersInfoStyles.formInputText,
                      {
                        backgroundColor: "white",
                        justifyContent: "center",
                        alignItems: "center",
                        marginBottom: 0,
                        color: this.state.originalityTextColor
                      }
                    ]}
                    numberOfLines={1}
                    ellipsizeMode={"tail"}
                    onPress={() => {
                      this.setModalVisible(true);
                      this.setState({ areaClickIndicator: "origin" });
                    }}
                  >
                    {this.state.PassportOriginality}
                  </Text>
                </View>
              ) : (
                <View
                  style={[
                    PassengersInfoStyles.passportContainerStyle,
                    {
                      backgroundColor: "white",
                      alignItems: "center",
                      borderRadius: 6,
                      height: 46,
                      flex: 0.5,
                      borderColor: "red",
                      borderWidth: this.state.bwNationalCode
                    }
                  ]}
                >
                  <TextInput
                    style={[
                      // PassengersInfoStyles.formInputText,
                      {
                        backgroundColor: "white",
                        borderRadius: 6,
                        flex: 1,
                        paddingHorizontal: 14,
                        fontSize: RFPercentage(2.2),
                        borderColor: "red",
                        // borderWidth: this.state.bwNationalCode,
                        height: Platform.OS === "ios" ? 46 : null
                      }
                    ]}
                    placeholder={this.props.NationalCode}
                    keyboardType="number-pad"
                    value={this.state.NationalCode}
                    ref={input => {
                      this.birthPlace = input;
                    }}
                    // onChangeText={text => (
                    //   this.updateValue(text, "PassportNumber"),
                    //   text.startsWith(" ")
                    //     ? this.setState({ PassportNumber: text.replace(" ", "") })
                    //     : this.setState({ bwPassNumber: 0 })
                    // )}
                    onChangeText={text => this.validateNationalCode(text)}
                  />
                </View>
              )}

              <View
                style={[
                  PassengersInfoStyles.passportContainerStyle,
                  {
                    backgroundColor: "white",
                    alignItems: "center",
                    borderRadius: 6,
                    marginLeft: 6,
                    height: 46,
                    flex: 0.5,
                    borderColor: "red",
                    borderWidth: this.state.bwBirthPlace
                  }
                ]}
              >
                <Text
                  style={[
                    PassengersInfoStyles.formInputText,
                    {
                      backgroundColor: "white",
                      justifyContent: "center",
                      alignItems: "center",
                      marginBottom: 0,
                      color: this.state.birthPlaceTextColor
                    }
                  ]}
                  numberOfLines={1}
                  ellipsizeMode={"tail"}
                  onPress={() => {
                    this.setModalVisible(true);
                    this.setState({ areaClickIndicator: "birthPlace" });
                  }}
                >
                  {this.state.PlaceOfBirth}
                </Text>
              </View>
            </View>

            {this.state.isInternational ? (
              <TouchableWithoutFeedback
                onPress={() => {
                  this.setDateModalVisible(true);
                }}
              >
                <View
                  style={[
                    PassengersInfoStyles.passportContainerStyle,
                    {
                      flex: 1,
                      backgroundColor: "white",
                      alignItems: "center",
                      borderRadius: 6,
                      marginBottom: 10,
                      borderColor: "red",
                      borderWidth: this.state.bwExpireDate,
                      height: Platform.OS === "ios" ? 46 : null
                      // height: 50
                    }
                  ]}
                >
                  <Text
                    style={[
                      PassengersInfoStyles.formInputText,
                      {
                        backgroundColor: "white",
                        justifyContent: "center",
                        alignItems: "center",
                        marginBottom: 0,
                        color: "darkgrey"
                      }
                    ]}
                    numberOfLines={1}
                    ellipsizeMode={"tail"}
                  >
                    {this.props.expireDate}
                  </Text>

                  <TextInput
                    style={[
                      PassengersInfoStyles.formInputText,
                      {
                        justifyContent: "center",
                        alignItems: "center",
                        textAlign: "center",
                        marginBottom: 0,
                        flex: 0,
                        width: 65,
                        color: "black"
                      }
                    ]}
                    editable={false}
                    placeholder="yyyy"
                    keyboardType="number-pad"
                    maxLength={4}
                    value={this.state.expireYear}
                  />

                  <View
                    style={[
                      { width: 2 },
                      { height: 10 },
                      { backgroundColor: "lightgrey" }
                    ]}
                  />

                  <TextInput
                    style={[
                      PassengersInfoStyles.formInputText,
                      {
                        justifyContent: "center",
                        alignItems: "center",
                        textAlign: "center",
                        marginBottom: 0,
                        flex: 0,
                        width: 56,
                        color: "black"
                      }
                    ]}
                    editable={false}
                    placeholder="mm"
                    keyboardType="number-pad"
                    maxLength={2}
                    value={this.state.expireMonth}
                  />

                  <View
                    style={[
                      { width: 2 },
                      { height: 10 },
                      { backgroundColor: "lightgrey" }
                    ]}
                  />

                  <TextInput
                    style={[
                      PassengersInfoStyles.formInputText,
                      {
                        justifyContent: "center",
                        alignItems: "center",
                        textAlign: "center",
                        marginBottom: 0,
                        flex: 0,
                        width: 56,
                        color: "black"
                      }
                    ]}
                    editable={false}
                    placeholder="dd"
                    keyboardType="number-pad"
                    maxLength={2}
                    value={this.state.expireDay}
                  />
                </View>
              </TouchableWithoutFeedback>
            ) : null}

            <TouchableWithoutFeedback
              onPress={() => {
                this.setBirthDateModalVisible(true);
              }}
            >
              <View
                style={[
                  PassengersInfoStyles.passportContainerStyle,
                  {
                    flex: 1,
                    backgroundColor: "white",
                    alignItems: "center",
                    justifyContent: "center",
                    borderRadius: 6,
                    marginBottom: 10,
                    borderColor: "red",
                    borderWidth: this.state.bwBirthDate,
                    height: Platform.OS === "ios" ? 46 : null
                  }
                ]}
              >
                <Text
                  style={[
                    PassengersInfoStyles.formInputText,
                    {
                      backgroundColor: "white",
                      justifyContent: "center",
                      textAlignVertical: "center",
                      alignItems: "center",
                      marginBottom: 0,
                      color: "darkgrey"
                    }
                  ]}
                  numberOfLines={1}
                  ellipsizeMode={"tail"}
                >
                  {this.props.birthDateTitle}
                </Text>

                <TextInput
                  style={[
                    PassengersInfoStyles.formInputText,
                    {
                      justifyContent: "center",
                      alignItems: "center",
                      textAlign: "center",
                      marginBottom: 0,
                      flex: 0,
                      width: 65,
                      color: "black"
                    }
                  ]}
                  editable={false}
                  placeholder="yyyy"
                  keyboardType="number-pad"
                  maxLength={4}
                  value={this.state.birthYear}
                />

                <View
                  style={[
                    { width: 2 },
                    { height: 10 },
                    { backgroundColor: "lightgrey" }
                  ]}
                />

                <TextInput
                  style={[
                    PassengersInfoStyles.formInputText,
                    {
                      justifyContent: "center",
                      alignItems: "center",
                      textAlign: "center",
                      marginBottom: 0,
                      flex: 0,
                      width: 56,
                      color: "black"
                    }
                  ]}
                  editable={false}
                  placeholder="mm"
                  keyboardType="number-pad"
                  maxLength={2}
                  value={this.state.birthMonth}
                />

                <View
                  style={[
                    { width: 2 },
                    { height: 10 },
                    { backgroundColor: "lightgrey" }
                  ]}
                />

                <TextInput
                  style={[
                    PassengersInfoStyles.formInputText,
                    {
                      justifyContent: "center",
                      alignItems: "center",
                      textAlign: "center",
                      marginBottom: 0,
                      flex: 0,
                      width: 56,
                      color: "black"
                    }
                  ]}
                  editable={false}
                  placeholder="dd"
                  keyboardType="number-pad"
                  maxLength={2}
                  value={this.state.birthDay}
                />
              </View>
            </TouchableWithoutFeedback>

            <View
              style={[
                PassengersInfoStyles.passportContainerStyle,
                { marginHorizontal: 40 },
                { marginTop: 0 }
              ]}
            >
              <Animated.View
                style={[
                  PassengersInfoStyles.formInputText,
                  { transform: [{ rotateX: spin }] },
                  { justifyContent: "center" },
                  { height: 44 },
                  { borderRadius: 10 },
                  { marginRight: 3 },
                  { backgroundColor: this.state.maleBtnColor }
                ]}
              >
                <TouchableOpacity
                  style={[{ justifyContent: "center" }, { height: 44 }]}
                  onPress={this.onMaleToggle}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ color: this.state.maleFontColor }}>
                      Male
                    </Text>
                    <View
                      style={{ backgroundColor: "transparent", width: 6 }}
                    />
                    <Icon
                      name="male"
                      color={this.state.maleIconColor}
                      style={{ fontSize: RFPercentage(4) }}
                    />
                  </View>
                </TouchableOpacity>
              </Animated.View>

              <Animated.View
                style={[
                  PassengersInfoStyles.formInputText,
                  { transform: [{ rotateX: spinFemale }] },
                  { justifyContent: "center" },
                  { height: 44 },
                  { marginLeft: 3 },
                  { borderRadius: 10 },
                  { backgroundColor: this.state.femaleBtnColor }
                ]}
              >
                <TouchableOpacity
                  style={[{ justifyContent: "center" }, { height: 44 }]}
                  onPress={this.onFemaleToggle}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ color: this.state.femaleFontColor }}>
                      Female
                    </Text>
                    <View
                      style={{ backgroundColor: "transparent", width: 6 }}
                    />
                    <Icon
                      name="female"
                      color={this.state.femaleIconColor}
                      style={{ fontSize: RFPercentage(4) }}
                    />
                  </View>
                </TouchableOpacity>
              </Animated.View>
            </View>

            <TouchableOpacity
              style={PassengersInfoStyles.btnNext}
              onPress={() =>
                this.state.nextPassenger == "مسافر بعدی"
                  ? this.onNextStep(payload)
                  : this.state.nextPassenger == "ذخیره تغییرات"
                  ? this.editPassenger()
                  : this.state.nextPassenger == "ثبت و ارسال"
                  ? this.sendPassengerInformation(payload)
                  : null
              }
            >
              <View style={{ alignItems: "center", flexDirection: "row" }}>
                {/* <Icon
                  name="back"
                  // color= "white"
                  style={{
                    fontSize: RFPercentage(2),
                    justifyContent: "center",
                    color: "#fff",
                    marginEnd: 10
                  }}
                /> */}

                <Text style={PassengersInfoStyles.btnNextText}>
                  {this.state.nextPassenger}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>

        <Icon name="group" style={PassengersInfoStyles.waterMark} />
      </LinearGradient>
    );
  }
}

PassengersInformation.propTypes = {
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
  birthPlace: PropTypes.string.isRequired,
  PassportOriginality: PropTypes.string.isRequired,
  NationalCode: PropTypes.string.isRequired,
  birthDateTitle: PropTypes.string.isRequired,
  passNumber: PropTypes.string.isRequired,
  expireDate: PropTypes.string.isRequired,
  male: PropTypes.string.isRequired,
  female: PropTypes.string.isRequired
};

PassengersInformation.defaultProps = {
  firstName: "First Name",
  lastName: "Last Name",
  birthPlace: "Place of Birth",
  PassportOriginality: "Passport Originality",
  NationalCode: "National code",
  birthDateTitle: "Date of Birth",
  passNumber: "Passport Number",
  expireDate: "Expire Date",
  male: "Male",
  female: "Female"
};

const PassengersInfoStyles = StyleSheet.create({
  infoTitleStyle: {
    fontFamily: "Sahel",
    color: "white",
    marginBottom: 8,
    fontSize: RFPercentage(2.2)
  },
  mainContainer: {
    textAlign: "center",
    flex: 1,
    // paddingLeft: 30,
    // paddingRight: 30,
    paddingHorizontal: "6%",
    paddingTop: 10
  },
  formInputText: {
    backgroundColor: "white",
    borderRadius: 6,
    marginBottom: 10,
    flex: 1,
    paddingHorizontal: 14,
    fontSize: RFPercentage(2.2),
    borderColor: "red"
  },
  passInputText: {
    backgroundColor: "white",
    padding: 10,
    borderRadius: 6,
    marginBottom: 6,
    flex: 1
  },
  dateInputTextStyle: {
    backgroundColor: "white",
    padding: 10,
    borderRadius: 6,
    marginLeft: 6,
    width: 64,
    textAlign: "center"
  },
  dateContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 6
  },
  datePickerStyle: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  passportContainerStyle: {
    flexDirection: "row",
    justifyContent: "space-around"
  },
  waterMark: {
    width: "100%",
    fontSize: RFPercentage(23),
    position: "absolute",
    bottom: 0,
    color: "#fff",
    zIndex: 0,
    opacity: 0.15
  },
  btnNext: {
    width: "100%",
    height: 45,
    backgroundColor: "orange",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 50,
    // paddingBottom: 7,
    marginTop: 20,
    marginBottom: 30,
    zIndex: -1
  },
  btnNextText: {
    color: "#1B275A",
    textAlign: "center",
    fontSize: RFPercentage(2.6),
    fontFamily: "Sahel-Bold",
    includeFontPadding: false
  },
  flatList: {
    backgroundColor: "#1B275A",
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: "grey"
  },
  menuOptionText: {
    fontFamily: "Sahel",
    // textAlign: "center",
    fontSize: RFPercentage(2.1),
    paddingVertical: 5,
    includeFontPadding: false
  }
});

function mapStateToProps(state) {
  return {
    adultsNumber: state.counter.adultsNumber,
    child: state.counter.child,
    newBorn: state.counter.newBorn
  };
}

export default connect(mapStateToProps)(PassengersInformation);
