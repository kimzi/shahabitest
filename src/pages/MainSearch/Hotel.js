import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  PixelRatio,
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import { Dimensions } from "react-native";
import AirLineSearchResult from "./AirLineSearchResult";
let deviceWidth = Dimensions.get("window").width;
let deviceHeight = Dimensions.get("window").height;
import BtnSearch from "../../components/BtnSearch";
import HotelFamilyContainer from "../../components/HotelFamilyContainer";
import WatermarkIcon from "../../components/WatermarkIcon";
import LightCalanderContainer from "../../components/LightCalanderContainer";
import FlightOptionsContainer from "../../components/FlightOptionsContainer";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

export default class Hotel extends Component {
  static navigationOptions = {
    header: {
      visible: false
    }
  };

  render() {
    return (
      <View style={[styles.container, { paddingTop: 37 }]}>
        <View style={styles.lightContainer}>
          <View style={[styles.destinationContainer]}>
            <View style={styles.containerHeader}>
              <Icon name="hotel" style={styles.containerIcon} />
              <Text style={styles.containerTitle}>مقصد</Text>
            </View>
            <View style={styles.destinationTextContainer}>
              <Text style={styles.airportName}>PARIS</Text>
              <Text style={styles.cityAirport}>France</Text>
            </View>
          </View>
        </View>

        <View style={styles.lightContainer}>
          <HotelFamilyContainer />
        </View>

        <LightCalanderContainer />
        <View style={{ height: "6%", marginTop: 10 }} />
        <TouchableOpacity
          style={[styles.BtnSearch]}
          onPress={() => this.props.navigaion.navigate("AirLineSearchResult")}
        >
          <BtnSearch />
        </TouchableOpacity>

        <WatermarkIcon />
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "#1B275A"
  },
  lightContainer: {
    height: PixelRatio.get() >=3 ? '23%' : "28%" ,
    width: "85%",
    alignItems: "center",
    backgroundColor: "#E9EEF6",
    borderRadius: 6,
    marginTop: 10
  },
  destinationContainer: {
    width: "90%",
    height: "100%"
  },
  destinationTextContainer: {
    alignItems: "center",
    textAlign: "center",
    justifyContent: "center",
    flex: 1
  },
  containerHeader: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    position:'absolute',
    top:0,
    width:'100%',
    zIndex:5
  },
  containerIcon: {
    color: "#91A9CD"
  },
  containerTitle: {
    fontFamily: "Sahel",
    color: "#91A9CD",
    fontSize: RFPercentage(1.8)
  },
  airportName: {
    color: "#15479F",
    fontFamily: "Montserrat-Bold",
    fontSize: RFPercentage(6),
    textAlign: "center"
  },
  cityAirport: {
    fontFamily: "Montserrat-Regular",
    fontSize: RFPercentage(1.8),
    color: "#91A9CD"
  },
  cityDetails: {
    fontFamily: "Montserrat-Regular",
    fontSize: RFPercentage(1.6),
    color: "#9EADC5"
  },
  BtnSearch: {
    width: "85%",
    height: "9%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    marginTop: 20
  }
});
