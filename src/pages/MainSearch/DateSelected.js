import React, { Component } from "react";
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import { Calendar } from "react-native-calendars";
import _ from "lodash";
import { LocaleConfig } from "react-native-calendars";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import WaterMark from "../../components/WatermarkIconBlack";
import BtnGradientCheckmark from "../../components/BtnGradientCheckmark";
import Cregorian from "./Cregorian";
import FlightOptions from "../../utilities/navigations/FlightOptions";
var moment = require("moment-jalaali");
moment().format("jYYYY-jM-jD");

let weekDay = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
let jWeekDay = [
  "یک‌شنبه",
  "دوشنبه",
  "سه‌شنبه",
  "چهارشنبه",
  "پنج‌شنبه",
  "جمعه",
  "شنبه"
];
let monthName = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sept",
  "Oct",
  "Nov",
  "Des"
];
let jMonthName = [
  "فروردین",
  "اردیبهشت",
  "خرداد",
  "تیر",
  "مرداد",
  "شهریور",
  "مهر",
  "آبان",
  "آذر",
  "دی",
  "بهمن",
  "اسفند"
];
export default class CalendarComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: null,
      finishDate: null,
      render: false,
      marks: {},
      markDay: {},
      calendarSelection: false,
      gregorianStart: null,
      jamaliStart: null,
      gregorianFinish: null,
      jamaliFinish: null,
      monthRender: 0,
      JamaliOneDaySelected: null,
      gregorianOneDaySelected: null,
      oneDateSelected: null
    };
  }

  async determineDay(date) {
    let firstYear = "";
    let firstMonth = "";
    let startDay = "";
    let secondYear = "";
    let secondMonth = "";
    let finishDay = "";
    let firstSplited = "";
    let secondSplited = "";

    let mark = {};
    if (this.state.calendarSelection) {
      await this.setState({
        calendarSelection: false,
        startDate: null,
        jStateDate: null,
        finishDate: null,
        jFinishDate: null,
        marks: {},
        markDay: {}
      });
    }
    if (this.state.startDate === null && this.state.calendarSelection != true) {
      firstSplited = date.split("-");
      firstYear = firstSplited[0];
      firstMonth = firstSplited[1];
      startDay = firstSplited[2];

      let d = new Date(date);
      m = moment(date, "YYYY-M-D");
      this.setState({
        startDate: date,
        jamaliStart: `(${jWeekDay[d.getDay()]}, ${
          m.format("jYYYY-jM-jD").split("-")[2]
        } ${jMonthName[m.format("jYYYY-jM-jD").split("-")[1] - 1]})`,
        gregorianStart: `${weekDay[d.getDay()]}, ${+startDay} ${
          monthName[+firstMonth - 1]
        }`,
        finishDate: null,
        jamaliFinish: null,
        gregorianFinish: null
      });
      mark[date] = {
        // endingDay: true,
        startingDay: true,
        selected: true,
        marked: true,
        color: "#15479F"
      };
      await _.merge(mark, this.state.marks);
      this.setState({ marks: mark });
      await this.setState({ render: 0 });
    } else if (
      this.state.finishDate === null &&
      this.state.calendarSelection != true
    ) {
      if (date < this.state.startDate) {
        firstSplited = date.split("-");
        firstYear = firstSplited[0];
        firstMonth = firstSplited[1];
        startDay = firstSplited[2];

        let d = new Date(date);
        m = moment(date, "YYYY-M-D");
        this.setState({
          startDate: date,
          jamaliStart: `(${jWeekDay[d.getDay()]}, ${
            m.format("jYYYY-jM-jD").split("-")[2]
          } ${jMonthName[m.format("jYYYY-jM-jD").split("-")[1] - 1]})`,
          gregorianStart: `${weekDay[d.getDay()]}, ${+startDay} ${
            monthName[+firstMonth - 1]
          }`,
          finishDate: null,
          jamaliFinish: null,
          gregorianFinish: null
        });

        mark[date] = {
          selected: true,
          marked: true,
          color: "#15479F",
          startingDay: true
          // endingDay: true
        };
        await this.setState({ marks: mark });
        return;
      }
      secondSplited = date.split("-");
      secondMonth = secondSplited[1];
      finishDay = secondSplited[2];

      let d = new Date(date);
      m = moment(date, "YYYY-M-D");
      this.setState({
        finishDate: date,
        jamaliFinish: `(${jWeekDay[d.getDay()]}, ${
          m.format("jYYYY-jM-jD").split("-")[2]
        } ${jMonthName[m.format("jYYYY-jM-jD").split("-")[1] - 1]})`,
        gregorianFinish: `${weekDay[d.getDay()]}, ${+finishDay} ${
          monthName[+secondMonth - 1]
        }`
      });

      await this.setState({
        finishDate: date,
        jFinishDate: m.format("jYYYY-jM-jD")
      });
      console.log("firstDay selected");
      mark[date] = {
        selected: true,
        marked: true,
        color: "#15479F",
        endingDay: true
        // startingDay: true
      };
      await _.merge(mark, this.state.marks);
      await this.setState({ marks: mark });
      secondSplited = this.state.finishDate.split("-");

      let firstSplited = this.state.startDate.split("-");

      secondYear = secondSplited[0];
      secondMonth = secondSplited[1];
      finishDay = secondSplited[2];
      firstYear = firstSplited[0];
      firstMonth = firstSplited[1];
      startDay = firstSplited[2];
      console.log(secondYear, secondMonth, finishDay);
      console.log();
      if (firstYear == secondYear) {
        let markCopy = this.state.marks;
        let counter = finishDay - startDay - 1;
        let date = "";
        if (firstMonth == secondMonth) {
          for (counter; counter > 0; counter -= 1) {
            if (+startDay + counter < 10) {
              date = `${firstYear}-${firstMonth}-0${+startDay + counter}`;
            } else {
              date = `${firstYear}-${firstMonth}-${+startDay + counter}`;
            }
            mark[date] = { selected: true, marked: true, color: "#6494E8" };
            _.merge(mark, markCopy);
          }
          await this.setState({ marks: mark });
          await this.setState({ render: true });
          await this.setState({ markDay: this.state.marks });
          this.setState({ calendarSelection: true });
        } else if (firstMonth < secondMonth) {
          console.log("firstMonth < secondMonth");
          let markCopy = this.state.marks;
          let counter = 32 - startDay;
          let date = "";
          for (counter; counter > 0; counter -= 1) {
            if (+startDay + counter < 10) {
              date = `${firstYear}-${firstMonth}-0${+startDay + counter}`;
            } else {
              date = `${firstYear}-${firstMonth}-${+startDay + counter}`;
            }
            mark[date] = { selected: true, marked: true, color: "#6494E8" };
            _.merge(mark, markCopy);
          }
          counter = 1;
          for (counter; counter < +finishDay; counter += 1) {
            if (counter < 10) {
              date = `${secondYear}-${secondMonth}-0${counter}`;
            } else {
              date = `${secondYear}-${secondMonth}-${counter}`;
            }
            mark[date] = { selected: true, marked: true, color: "#6494E8" };
            _.merge(mark, markCopy);
          }
          if (secondMonth - firstMonth != 0) {
            counter = +firstMonth + 1;
            let i = 1;
            for (counter; counter < secondMonth; counter++) {
              for (i = 1; i <= 31; i++) {
                console.log("counter", counter, "i", i);
                if (counter < 10) {
                  if (i < 10) {
                    date = `${secondYear}-0${counter}-0${i}`;
                  } else {
                    date = `${secondYear}-0${counter}-${i}`;
                  }
                } else {
                  if (i < 10) {
                    date = `${secondYear}-${counter}-0${i}`;
                  } else {
                    date = `${secondYear}-${counter}-${i}`;
                  }
                }
                if (i < 10) {
                  date = `${secondYear}-0${counter}-0${i}`;
                } else {
                  date = `${secondYear}-0${counter}-${i}`;
                }
                mark[date] = { selected: true, marked: true, color: "#6494E8" };
                _.merge(mark, markCopy);
              }
            }
            await this.setState({ marks: mark });
            await this.setState({ render: true });
            await this.setState({ markDay: this.state.marks });
            console.log(this.state.markDay);
            this.setState({ calendarSelection: true });
          }
        }
      } else {
        let markCopy = this.state.marks;
        counter = +firstMonth;
        i = +startDay + 1;
        for (counter; counter <= 12; counter += 1) {
          if (this.state.monthRender == 1) {
            i = 0;
          } else {
            this.setState({ monthRender: 1 });
          }
          for (i; i <= 31; i++) {
            console.log("counter", counter, "i", i);
            if (counter < 10) {
              if (i < 10) {
                date = `${firstYear}-0${counter}-0${i}`;
              } else {
                date = `${firstYear}-0${counter}-${i}`;
              }
            } else {
              if (i < 10) {
                date = `${firstYear}-${counter}-0${i}`;
              } else {
                date = `${firstYear}-${counter}-${i}`;
              }
            }
            mark[date] = { selected: true, marked: true, color: "#6494E8" };
          }
        }

        for (counter = 1; counter <= secondMonth; counter++) {
          let j;
          if (counter == secondMonth) {
            j = finishDay - 1;
          } else {
            j = 31;
          }
          console.log("start this year :)))))))))))))))))");
          for (i = 1; i <= j; i++) {
            console.log("counter", counter, "i", i);
            if (counter < 10) {
              if (i < 10) {
                date = `${secondYear}-0${counter}-0${i}`;
              } else {
                date = `${secondYear}-0${counter}-${i}`;
              }
            } else {
              if (i < 10) {
                date = `${secondYear}-${counter}-0${i}`;
              } else {
                date = `${secondYear}-${counter}-${i}`;
              }
            }
            mark[date] = { selected: true, marked: true, color: "#6494E8" };
            _.merge(mark, markCopy);
          }
        }
        await this.setState({ marks: mark });
        await this.setState({ render: true });
        await this.setState({ markDay: this.state.marks });
        console.log(this.state.markDay);
        this.setState({ calendarSelection: true });
      }
    } else {
      this.forceUpdate();
    }
  }
  async oneDaySelected(date) {
    const Splited = date.split("-");
    year = Splited[0];
    month = Splited[1];
    Day = Splited[2];

    let d = new Date(date);
    m = moment(date, "YYYY-M-D");
    this.setState({
      JamaliOneDaySelected: `(${jWeekDay[d.getDay()]}, ${
        m.format("jYYYY-jM-jD").split("-")[2]
      } ${jMonthName[m.format("jYYYY-jM-jD").split("-")[1] - 1]})`,
      gregorianOneDaySelected: `${weekDay[d.getDay()]}, ${+Day} ${
        monthName[+month - 1]
      }`,
      oneDateSelected: date
    });
    let mark = {};
    mark[date] = {
      startingDay: true,
      endingDay: true,
      selected: true,
      marked: true,
      color: "#15479F"
    };
    this.setState({ markDay: mark });
    await this.setState({ render: 0 });
  }
  onPress(sourceCode, receivedValue) {
    if (sourceCode == 0) {
      this.state.startDate && this.state.finishDate != null
        ? // this.props.navigation.navigate(
          //     "FlightOptions",
          //     {
          //       startDay: this.state.startDate,
          //       finishDate: this.state.finishDate,
          //       jStartDate: this.state.jamaliStart,
          //       gStartDate: this.state.gregorianStart,
          //       jFinishDate: this.state.jamaliFinish,
          //       gFinishDate: this.state.gregorianFinish
          //     },
          //     console.log(this.startDate)
          //   )
          (receivedValue(this.state.gregorianStart, this.state.gregorianFinish),
          this.props.navigation.goBack())
        : alert("select your start and finish date");
    } else if (sourceCode == 1) {
      this.state.oneDateSelected != null
        ? //  this.props.navigation.navigate(
          //     "FlightOptions",
          //     {
          //       JamaliOneDaySelected: this.state.JamaliOneDaySelected,
          //       gregorianOneDaySelected: this.state.gregorianOneDaySelected,
          //       oneDateSelected: this.state.oneDateSelected,
          //       dateWasSelected: 1
          //     },
          //     console.log(this.state.JamaliOneDaySelected)
          //   )
          (receivedValue(this.state.gregorianOneDaySelected),
          this.props.navigation.goBack())
        : alert("select your date");
    }
  }

  receivedValue = calanderValue => {
    this.setState({ calanderValue });
  };

  render() {
    const receivedValue = this.props.navigation.getParam(
      "receivedValue",
      () => {}
    );
    const { navigation } = this.props;
    const sourceCode = navigation.getParam("sourceCode", "sourceCode");
    return (
      <View style={styles.container}>
        <View>
          <Icon
            name="back"
            color="#000000"
            style={styles.backIcon}
            onPress={() => this.props.navigation.navigate("FlightOptions")}
          />
        </View>
        <View style={{ justifyContent: "flex-start" }}>
          <View>
            <View style={styles.headerTextContainer}>
              <Text style={styles.headerText}>
                روزهای رفت و برگشت خود را انتخاب کنید
              </Text>
            </View>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("Cregorian", {
                    receivedValue: this.receivedValue,
                    sourceCode: sourceCode
                  })
                }
                style={{
                  marginTop: 10,
                  justifyContent: "center",
                  width: "25%",
                  borderWidth: 1,
                  borderColor: "#F09819",
                  borderRadius: 20
                }}
              >
                <View style={{}}>
                  <Text
                    style={{
                      textAlign: "center",
                      alignSelf: "center",
                      color: "#F09819",
                      fontSize: RFPercentage(1.9),
                      fontFamily: "Sahel"
                    }}
                  >
                    {/* تقویم شمسی */}
                    {this.state.calanderValue}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.calanderContainer}>
              {sourceCode == 0 ? (
                <Calendar
                  monthFormat={" MMMM yyyy"}
                  onMonthChange={month => {
                    console.log("month changed", month);
                  }}
                  hideArrows={false}
                  hideExtraDays={false}
                  disableMonthChange={true}
                  startDate={1}
                  hideDayNames={false}
                  hideArrows={false}
                  renderArrow={direction => <Icon name={direction} size={20} />}
                  showWeekNumbers={false}
                  onPressArrowLeft={substractMonth => substractMonth()}
                  onPressArrowRight={addMonth => addMonth()}
                  onDayPress={day => this.determineDay(day.dateString)}
                  markedDates={
                    this.state.render ? this.state.markDay : this.state.marks
                  }
                  markingType={"period"}
                  theme={{
                    textSectionTitleColor: "#7A7F85",
                    selectedDayBackgroundColor: "#00adf5",
                    selectedDayTextColor: "#ffffff",
                    todayTextColor: "#B6BEC6",
                    dayTextColor: "#B6BEC6",
                    textDisabledColor: "#B6BEC6",
                    dotColor: "red",
                    selectedDotColor: "#ffffff",
                    arrowColor: "orange",
                    monthTextColor: "#15479F",
                    textDayFontFamily: "Montserrat-Regular",
                    textMonthFontFamily: "Montserrat-Regular",
                    textDayHeaderFontFamily: "Montserrat-Regular",
                    textDayFontSize: RFPercentage(2.2),
                    textMonthFontSize: RFPercentage(2.9),
                    textDayHeaderFontSize: RFPercentage(2.2)
                  }}
                />
              ) : (
                <Calendar
                  monthFormat={" MMMM yyyy"}
                  onMonthChange={month => {
                    console.log("month changed", month);
                  }}
                  hideArrows={false}
                  hideExtraDays={false}
                  disableMonthChange={true}
                  startDate={1}
                  hideDayNames={false}
                  hideArrows={false}
                  renderArrow={direction => <Icon name={direction} size={20} />}
                  showWeekNumbers={false}
                  onPressArrowLeft={substractMonth => substractMonth()}
                  onPressArrowRight={addMonth => addMonth()}
                  onDayPress={day => this.oneDaySelected(day.dateString)}
                  markedDates={this.state.markDay}
                  markingType={"period"}
                  theme={{
                    textSectionTitleColor: "#7A7F85",
                    selectedDayBackgroundColor: "#00adf5",
                    selectedDayTextColor: "#ffffff",
                    todayTextColor: "#B6BEC6",
                    dayTextColor: "#B6BEC6",
                    textDisabledColor: "#B6BEC6",
                    dotColor: "red",
                    selectedDotColor: "#ffffff",
                    arrowColor: "orange",
                    monthTextColor: "#15479F",
                    textDayFontFamily: "Montserrat-Regular",
                    textMonthFontFamily: "Montserrat-Regular",
                    textDayHeaderFontFamily: "Montserrat-Regular",
                    textDayFontSize: RFPercentage(2.2),
                    textMonthFontSize: RFPercentage(2.9),
                    textDayHeaderFontSize: RFPercentage(2.2)
                  }}
                />
              )}
            </View>
          </View>
          {sourceCode == 0 ? (
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  marginLeft: 24
                }}
              >
                <Text style={[styles.dateText]}>
                  {this.state.gregorianStart ? "تاریخ رفت" : null}
                </Text>

                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text style={styles.EnDetails}>
                    {this.state.gregorianStart}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row-reverse",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text style={styles.Pdetails}>{this.state.jamaliStart}</Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: "column",
                  marginRight: 24,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text style={[styles.dateText]}>
                  {this.state.gregorianFinish ? "تاریخ برگشت" : null}
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text style={styles.EnDetails}>
                    {this.state.gregorianFinish}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row-reverse",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text style={styles.Pdetails}>{this.state.jamaliFinish}</Text>
                </View>
              </View>
            </View>
          ) : (
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text style={[styles.dateText]}>
                  {this.state.gregorianOneDaySelected ? "تاریخ" : null}{" "}
                </Text>
                <Text style={styles.EnDetails}>
                  {this.state.gregorianOneDaySelected}
                </Text>
                <Text style={styles.Pdetails}>
                  {this.state.JamaliOneDaySelected}
                </Text>
              </View>
            </View>
          )}
        </View>
        <WaterMark />
        <TouchableOpacity
          style={{
            width: 150,
            height: "6%",
            position: "absolute",
            bottom: 20,
            alignSelf: "center"
          }}
          onPress={() => this.onPress(sourceCode, receivedValue)}
        >
          <BtnGradientCheckmark />
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  backIcon: {
    padding: 16,
    fontSize: RFPercentage(2.2),
    width: 40,
    height: 30,

  },
  headerText: {
    color: "#15479F",
    fontFamily: "Sahel",
    fontSize: RFPercentage(2.2),

  },
  headerTextContainer: {
    alignItems: "center"
  },
  calanderContainer: {
    marginLeft: 20,
    marginRight: 20,
    height: "65%"
  },
  dateText: {
    fontFamily: "Sahel",
    fontSize: 15,
    color: "#15479F"
  },
  EnDetails: {
    fontFamily: "Montserrat-Medium",
    fontSize: RFPercentage(2.1),
    color: "#2C3039"
  },
  Pdetails: {
    fontFamily: "Sahel",
    fontSize: RFPercentage(2.2),
    color: "#2C3039"
  }
});
