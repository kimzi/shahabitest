import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  Button,
  Image
} from "react-native";
import BtnGradient from "../../components/BtnGradientCheckmark";
import LinearGradient from "react-native-linear-gradient";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import _ from "lodash";
import { Calendar, CalendarList, Agenda } from "react-native-calendars-persian";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { LocaleConfig } from "react-native-calendars-persian";
import WaterMark from "../../components/WatermarkIconBlack";
import calander from "./oneWayCalendar";
import FlightOptions from "../../utilities/navigations/FlightOptions";
import BtnGradientCheckmark from "../../components/BtnGradientCheckmark";

var moment = require("moment-jalaali");
var jmoment = require("moment");
moment().format("jYYYY-jM-jD");
const XDate = require("xdate");

const cache = {
  sameMonth: {},
  dates: {},
  months: {}
};

let isIntlSupported =
  typeof Intl !== "undefined" && typeof Intl.DateTimeFormat === "function";

// const moment = require("moment-jalaali");
let weekDay = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
let jWeekDay = [
  "یک‌شنبه",
  "دوشنبه",
  "سه‌شنبه",
  "چهارشنبه",
  "پنج‌شنبه",
  "جمعه",
  "شنبه"
];
let monthName = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sept",
  "Oct",
  "Nov",
  "Des"
];
let jMonthName = [
  "فروردین",
  "اردیبهشت",
  "خرداد",
  "تیر",
  "مرداد",
  "شهریور",
  "مهر",
  "آبان",
  "آذر",
  "دی",
  "بهمن",
  "اسفند"
];

export default class CalendarComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: null,
      finishDate: null,
      render: false,
      marks: {},
      markDay: {},
      calendarSelection: false,
      gregorianStart: null,
      jamaliStart: null,
      gregorianFinish: null,
      jamaliFinish: null,
      monthRender: 0,
      JamaliOneDaySelected: null,
      gregorianOneDaySelected: null,
      oneDateSelected: null,
      calanderCode: 1,
      todayDate:null,
    };
  }
  componentDidMount(){
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();

    today = yyyy + '-' + mm + '-' + dd;
    this.setState({todayDate : today})

  }

  updateParentState(
    oneDateSelected,
    gregorianOneDaySelected,
    JamaliOneDaySelected
  ) {
    this.props.updateParentState(
      oneDateSelected,
      gregorianOneDaySelected,
      JamaliOneDaySelected
    );
  }

  async oneDaySelected(date) {
    const Splited = date.split("-");
    year = Splited[0];
    month = Splited[1];
    Day = Splited[2];

    let d = new Date(date);
    m = moment(date, "YYYY-M-D");
    this.setState({
      JamaliOneDaySelected: `(${jWeekDay[d.getDay()]}, ${
        m.format("jYYYY-jM-jD").split("-")[2]
      } ${jMonthName[m.format("jYYYY-jM-jD").split("-")[1] - 1]})`,
      gregorianOneDaySelected: `${weekDay[d.getDay()]}, ${+Day} ${
        monthName[+month - 1]
      }`,
      oneDateSelected: date
    });
    let mark = {};
    mark[date] = {
      selected: true,
      marked: true,
      color: "#15479F",
      startingDay: true,
      endingDay: true
    };
    this.setState({ markDay: mark });
    await this.setState({ render: 0 });
    this.updateParentState(
      this.state.oneDateSelected,
      this.state.gregorianOneDaySelected,
      this.state.JamaliOneDaySelected
    );
  }

  render() {
    moment.updateLocale("fa");
    return (
      <View style={{ flex: 1 }}>
        <Calendar
                          minDate={this.state.todayDate}

          jalali={true}
          monthFormat={" MMMM yyyy"}
          onMonthChange={month => {
            console.log("month changed", month);
          }}
          hideArrows={false}
          hideExtraDays={false}
          disableMonthChange={true}
          startDate={1}
          hideDayNames={false}
          hideArrows={false}
          renderArrow={direction => <Icon name={direction} size={20} />}
          showWeekNumbers={false}
          onPressArrowLeft={substractMonth => substractMonth()}
          onPressArrowRight={addMonth => addMonth()}
          onDayPress={day => this.oneDaySelected(day.dateString)}
          markedDates={this.state.markDay}
          // start Day from shanbe
          firstDay={6}
          markingType={"period"}
          theme={{
            "stylesheet.calendar.header": {
              week: {
                flexDirection: "row-reverse",
                justifyContent: "space-around"
              }
            },
            "stylesheet.calendar.main": {
              week: {
                flexDirection: "row-reverse",
                justifyContent: "space-between",
                marginTop: 5
              }
            },
            'stylesheet.day.period': {
              base: {
                overflow: 'hidden',
                height: 35,
                alignItems: 'center',
                justifyContent : 'center',
                width: 35,
              }
          },        
            textSectionTitleColor: "#7A7F85",
            selectedDayBackgroundColor: "#00adf5",
            selectedDayTextColor: "#ffffff",
            todayTextColor: "#15479F",
            dayTextColor: "#B6BEC6",
            textDisabledColor: "#B6BEC6",
            dotColor: "red",
            selectedDotColor: "#ffffff",
            arrowColor: "orange",
            monthTextColor: "#15479F",
            textDayFontFamily: "Montserrat-Regular",
            textMonthFontFamily: "Montserrat-Regular",
            textDayHeaderFontFamily: "Montserrat-Regular",
            textDayFontSize: RFPercentage(3),
            textMonthFontSize: RFPercentage(2.9),
            textDayHeaderFontSize: RFPercentage(2.2)
          }}
        />
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <View
            style={{
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={[styles.dateText]}>
              {this.state.gregorianOneDaySelected ? "تاریخ" : null}{" "}
            </Text>
            <Text style={styles.EnDetails}>
              {this.state.gregorianOneDaySelected}
            </Text>
            <Text style={styles.Pdetails}>
              {this.state.JamaliOneDaySelected}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  backIcon: {
    padding: 16,
    fontSize: RFPercentage(2.2)
  },
  headerText: {
    color: "#15479F",
    fontFamily: "Sahel",
    fontSize: RFPercentage(2.2)
  },
  headerTextContainer: {
    alignItems: "center"
  },
  calanderContainer: {
    marginLeft: 20,
    marginRight: 20,
    height: "65%"
  },
  dateText: {
    fontFamily: "Sahel",
    fontSize: 15,
    color: "#15479F"
  },
  EnDetails: {
    fontFamily: "Montserrat-Medium",
    fontSize: RFPercentage(2.1),
    color: "#2C3039"
  },
  Pdetails: {
    fontFamily: "Sahel",
    fontSize: RFPercentage(2.2),
    color: "#2C3039"
  }
});
