import React, { Component } from "react";
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import { Calendar } from "react-native-calendars";
import _ from "lodash";
import { LocaleConfig } from "react-native-calendars";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import WaterMark from "../../components/WatermarkIconBlack";
import BtnGradientCheckmark from "../../components/BtnGradientCheckmark";
import Cregorian from "./Cregorian";
import FlightOptions from "../../utilities/navigations/FlightOptions";
var moment = require("moment-jalaali");
moment().format("jYYYY-jM-jD");

let weekDay = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
let jWeekDay = [
  "یک‌شنبه",
  "دوشنبه",
  "سه‌شنبه",
  "چهارشنبه",
  "پنج‌شنبه",
  "جمعه",
  "شنبه"
];
let monthName = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sept",
  "Oct",
  "Nov",
  "Des"
];
let jMonthName = [
  "فروردین",
  "اردیبهشت",
  "خرداد",
  "تیر",
  "مرداد",
  "شهریور",
  "مهر",
  "آبان",
  "آذر",
  "دی",
  "بهمن",
  "اسفند"
];
LocaleConfig.locales.en = LocaleConfig.locales[""];

LocaleConfig.locales["fr"] = {
  monthNames: [
    "January",
    "February ",
    "March ",
    "April ",
    "May ",
    "June ",
    "July ",
    "August ",
    "September ",
    "October ",
    "November ",
    "December "
  ],
  monthNamesShort: [
    "Janv.",
    "Févr.",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juil.",
    "Août",
    "Sept.",
    "Oct.",
    "Nov.",
    "Déc."
  ],
  dayNames: [
    "Sunday",
    "Monday	",
    "Tuesday",
    "Wednesday	",
    "Thursday",
    "Friday",
    "Saturday"
  ],
  dayNamesShort: ["S", "M", "T", "W", "T", "F", "S"]
};

LocaleConfig.defaultLocale = "fr";
export default class CalendarComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: null,
      finishDate: null,
      render: false,
      marks: {},
      markDay: {},
      calendarSelection: false,
      gregorianStart: null,
      jamaliStart: null,
      gregorianFinish: null,
      jamaliFinish: null,
      monthRender: 0,
      JamaliOneDaySelected: null,
      gregorianOneDaySelected: null,
      oneDateSelected: null,
      calanderCode: 1,
      todayDate : null,
    };
  }
  componentDidMount(){
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();

    today = yyyy + '-' + mm + '-' + dd;
    this.setState({todayDate : today})

  }

  updateParentState(
    oneDateSelected,
    gregorianOneDaySelected,
    JamaliOneDaySelected
  ) {
    this.props.updateParentState(
      oneDateSelected,
      gregorianOneDaySelected,
      JamaliOneDaySelected
    );
  }

  async oneDaySelected(date) {
    const Splited = date.split("-");
    year = Splited[0];
    month = Splited[1];
    Day = Splited[2];

    let d = new Date(date);
    m = moment(date, "YYYY-M-D");
    this.setState({
      JamaliOneDaySelected: `(${jWeekDay[d.getDay()]}, ${
        m.format("jYYYY-jM-jD").split("-")[2]
      } ${jMonthName[m.format("jYYYY-jM-jD").split("-")[1] - 1]})`,
      gregorianOneDaySelected: `${weekDay[d.getDay()]}, ${+Day} ${
        monthName[+month - 1]
      }`,
      oneDateSelected: date
    });
    let mark = {};
    mark[date] = {
      selected: true,
      marked: true,
      color: "#15479F",
      startingDay: true,
      endingDay: true
    };
    this.setState({ markDay: mark });
    await this.setState({ render: 0 });
    this.updateParentState(
      this.state.oneDateSelected,
      this.state.gregorianOneDaySelected,
      this.state.JamaliOneDaySelected
    );
  }

  render() {
    LocaleConfig.locales.fr = LocaleConfig.locales[""];

    LocaleConfig.locales["en"] = {
      monthNames: [
        "January",
        "February ",
        "March ",
        "April ",
        "May ",
        "June ",
        "July ",
        "August ",
        "September ",
        "October ",
        "November ",
        "December "
      ],
      monthNamesShort: [
        "Janv.",
        "Févr.",
        "Mars",
        "Avril",
        "Mai",
        "Juin",
        "Juil.",
        "Août",
        "Sept.",
        "Oct.",
        "Nov.",
        "Déc."
      ],
      dayNames: [
        "Sunday",
        "Monday	",
        "Tuesday",
        "Wednesday	",
        "Thursday",
        "Friday",
        "Saturday"
      ],
      dayNamesShort: ["S", "M", "T", "W", "T", "F", "S"]
    };

    LocaleConfig.defaultLocale = "en";

    return (
      <View style={{ flex: 1 }}>
        <Calendar
                  minDate={this.state.todayDate}

          monthFormat={" MMMM yyyy"}
          onMonthChange={month => {
            console.log("month changed", month);
          }}
          hideArrows={false}
          hideExtraDays={false}
          disableMonthChange={true}
          startDate={1}
          hideDayNames={false}
          hideArrows={false}
          renderArrow={direction => <Icon name={direction} size={20} />}
          showWeekNumbers={false}
          onPressArrowLeft={substractMonth => substractMonth()}
          onPressArrowRight={addMonth => addMonth()}
          onDayPress={day => this.oneDaySelected(day.dateString)}
          markedDates={this.state.markDay}
          markingType={"period"}
          theme={{
            textSectionTitleColor: "#7A7F85",
            selectedDayBackgroundColor: "#00adf5",
            selectedDayTextColor: "#ffffff",
            todayTextColor: "#15479F",
            dayTextColor: "#B6BEC6",
            textDisabledColor: "#B6BEC6",
            dotColor: "red",
            selectedDotColor: "#ffffff",
            arrowColor: "orange",
            monthTextColor: "#15479F",
            textDayFontFamily: "Montserrat-Regular",
            textMonthFontFamily: "Montserrat-Regular",
            textDayHeaderFontFamily: "Montserrat-Regular",
            textDayFontSize: RFPercentage(2.2),
            textMonthFontSize: RFPercentage(2.9),
            textDayHeaderFontSize: RFPercentage(2.2),
            'stylesheet.day.period': {
              base: {
                overflow: 'hidden',
                height: 35,
                alignItems: 'center',
                width: 35,
              }
          }
        
          }}
        />
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <View
            style={{
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={[styles.dateText]}>
              {this.state.gregorianOneDaySelected ? "تاریخ" : null}{" "}
            </Text>
            <Text style={styles.EnDetails}>
              {this.state.gregorianOneDaySelected}
            </Text>
            <Text style={styles.Pdetails}>
              {this.state.JamaliOneDaySelected}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  backIcon: {
    padding: 16,
    fontSize: RFPercentage(2.2)
  },
  headerText: {
    color: "#15479F",
    fontFamily: "Sahel",
    fontSize: RFPercentage(2.2)
  },
  headerTextContainer: {
    alignItems: "center"
  },
  calanderContainer: {
    marginLeft: 20,
    marginRight: 20,
    height: "65%"
  },
  dateText: {
    fontFamily: "Sahel",
    fontSize: 15,
    color: "#15479F"
  },
  EnDetails: {
    fontFamily: "Montserrat-Medium",
    fontSize: RFPercentage(2.1),
    color: "#2C3039"
  },
  Pdetails: {
    fontFamily: "Sahel",
    fontSize: RFPercentage(2.2),
    color: "#2C3039"
  }
});
