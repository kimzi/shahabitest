import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  Image,
  AsyncStorage,
  StatusBar
} from "react-native";
import WalkThrough from "./WalkThrough";
import LinearGradient from "react-native-linear-gradient";
// import DeviceInfo from "react-native-device-info";
const axios = require("axios");
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import FlightOptions from "../utilities/navigations/FlightOptions";
import {
  IS_WALKTHROUGH,
  IS_LOGIN_POSTPONED,
  USER_ID
} from "../constants/UserParams";

export default class Splash extends Component {
  static navigationOptions = { header: null };
  constructor(props) {
    super(props);
    this.state = {
      firstLaunch: null
    };
  }

  componentDidMount = async () => {
    try {
      const walkThrough = await AsyncStorage.getItem(IS_WALKTHROUGH);
      const isLoginPostponed = await AsyncStorage.getItem(IS_LOGIN_POSTPONED);
      const userId = await AsyncStorage.getItem(USER_ID);
      console.log(
        "walkThrough",
        walkThrough,
        "isLoginPostponed",
        isLoginPostponed,
        "userId",
        userId
      );
      if (walkThrough == null) {
        setTimeout(() => {
          this.props.navigation.navigate("WalkThrough");
        }, 1000);
        console.log(value);
      } else if (userId == null && isLoginPostponed == null) {
        setTimeout(() => {
          this.props.navigation.navigate("Login", { fromAvailable: "false" });
        }, 1000);
      } else if (userId == null && isLoginPostponed) {
        setTimeout(() => {
          this.props.navigation.navigate("Drawer");
        }, 1000);
      } else if (userId && isLoginPostponed == null) {
        setTimeout(() => {
          this.props.navigation.navigate("Drawer");
        }, 1000);
      } else {
        // setTimeout(() => {this.props.navigation.navigate('WalkThrough')}, 1000)
        setTimeout(() => {
          this.props.navigation.navigate("Drawer");
        }, 1000);
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  // let config = {
  //   headers: {
  //     Device_Key: Base64.stringify(
  //       CryptoJS.SHA512("$%$%%^%^saMseirkeY$%$%%^%^", "X2")
  //     ).toUpperCase()
  //   }
  // };
  // axios
  //   .post(
  //     "http://192.168.31.196:47173/api/flight/Airport_Search",
  //     {
  //       Airport: "تهر"
  //       // {
  //     //   Brand: DeviceInfo.getBrand(),
  //     //   Carrier: DeviceInfo.getCarrier(),
  //     //   DeviceId: DeviceInfo.getDeviceId(),
  //     //   InstanceID: DeviceInfo.getInstanceID(),
  //     //   Manufacturer: DeviceInfo.getManufacturer(),
  //     //   Model: DeviceInfo.getModel(),
  //     //   ReadableVersion: DeviceInfo.getReadableVersion(),
  //     //   SerialNumber: DeviceInfo.getSerialNumber(),
  //     //   SystemName: DeviceInfo.getSystemName(),
  //     //   SystemVersion: DeviceInfo.getSystemVersion(),
  //     //   TimeZone: DeviceInfo.getTimezone(),
  //     //   UniqueID: DeviceInfo.getUniqueID(),
  //     //   UserAgent: DeviceInfo.getUserAgent()
  //     // },
  //     // config
  //     },
  //     // config
  //   )
  //   .then(function(response) {
  //     console.warn(response.data, "akrhgirhroh");
  //   })
  //   .catch(function(error) {
  //     console.alert(error);
  //   });

  // setTimeout(() => {
  //   AsyncStorage.getItem("uniqueId").then(value => {
  //     if (value == null) {
  //       console.log("value is null");
  //       AsyncStorage.setItem("uniqueId", DeviceInfo.getUniqueID());
  //       this.setState(
  //         { firstLaunch: true },
  //         console.log(this.state.firstLaunch)
  //       );
  //       this.props.navigation.navigate("WalkThrough");
  //     } else {
  //       console.log("value is not null"),
  //         this.setState(
  //           { firstLaunch: false },
  //           console.log(this.state.firstLaunch)
  //         );
  //         //edit this
  //         this.props.navigation.navigate("WalkThrough");
  //     }
  //   });
  // }, 1500);}
  render() {
    return (
      <LinearGradient
        colors={["#15479F", "#18377D", "#1B275A"]}
        style={styles.container}
      >
        <StatusBar backgroundColor="#15479F" />
        <View style={styles.logoContainer}>
          <Icon
            name="logo"
            size={80}
            color="#fff"
            style={{ marginBottom: 10 }}
          />
          <Icon name="typography" size={60} color="#fff" />
        </View>

        <Icon name="group" size={150} color="#fff" style={styles.group} />
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#1B275A",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center"
  },
  logoContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  group: {
    opacity: 0.35
  }
});
