import { AsyncStorage } from "react-native";
import sha512 from "crypto-js/sha256";
var CryptoJS = require("crypto-js");

var ciphertext = CryptoJS.AES.encrypt("samsair", "x2");
console.log("encrypted text", ciphertext.toString());

const deviceStorage = {
  async saveItem(key, value) {
    try {
      await AsyncStorage.setItem(key, value);
    } catch (error) {
      console.log("AsyncStorage Error: " + error.message);
    }
  }
};

export default deviceStorage;
