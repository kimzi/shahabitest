import {
  COUNTER_PLUS_ADULT,
  COUNTER_MINUS_ADULT,
  COUNTER_PLUS_CHILD,
  COUNTER_MINUS_CHILD,
  COUNTER_PLUS_NEWBORN,
  COUNTER_MINUS_NEWBORN
} from "../constants/ActionTypes";
import initialState from "./initialState";

const counter = (states = initialState, action) => {
  switch (action.type) {
    case COUNTER_PLUS_ADULT:
      // console.warn(states);
      return {
        adultsNumber: states.adultsNumber + 1,
        child: states.child,
        newBorn: states.newBorn
      };
    case COUNTER_MINUS_ADULT:
      if (states.adultsNumber !== 1) {
        return {
          adultsNumber: states.adultsNumber - 1,
          child: states.child,
          newBorn: states.newBorn
        };
      } else {
        return {
          adultsNumber: states.adultsNumber,
          child: states.child,
          newBorn: states.newBorn
        };
      }
    case COUNTER_PLUS_CHILD:
      return {
        adultsNumber: states.adultsNumber,
        child: states.child + 1,
        newBorn: states.newBorn
      };
    case COUNTER_MINUS_CHILD:
      if (states.child !== 0) {
        return {
          adultsNumber: states.adultsNumber,
          child: states.child - 1,
          newBorn: states.newBorn
        };
      } else {
        return {
          adultsNumber: states.adultsNumber,
          child: states.child,
          newBorn: states.newBorn
        };
      }
    case COUNTER_PLUS_NEWBORN:
      return {
        adultsNumber: states.adultsNumber,
        child: states.child,
        newBorn: states.newBorn + 1
      };
    case COUNTER_MINUS_NEWBORN:
      if (states.newBorn !== 0) {
        return {
          adultsNumber: states.adultsNumber,
          child: states.child,
          newBorn: states.newBorn - 1
        };
      } else {
        return {
          adultsNumber: states.adultsNumber,
          child: states.child,
          newBorn: states.newBorn
        };
      }
    default:
      return states;
  }
};
export default counter;
