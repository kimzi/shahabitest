import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  TouchableOpacity
} from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
const Icon = createIconSetFromFontello(fontelloConfig);

export default class lightCalanderContainer extends Component {
  render() {
    return (
      <View style={styles.lightCalanderContainer}>
        <View style={[styles.calanderContainer]}>
          <View style={styles.containerHeader}>
            <Icon name="calander" style={styles.containerIcon} />
            <Text style={styles.containerTitle}>زمانبندی</Text>
          </View>
          {!this.props.date && !this.props.gregorianStart ? (
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                
              }}
            >
              <Text
                style={{
                  fontFamily: "Sahel-Bold",
                  fontSize: RFPercentage(2),
                  color: "#15479F",
                }}
              >
                انتخاب تاریخ
              </Text>
            </View>
          ) : null}
          {this.props.date == null ? null : (
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "row",
                flex:1
              }}
            >
              <Icon
                name="departure"
                size={15}
                style={{ color: "#15479F", paddingRight: 5 }}
              />
              {/* <Text>{this.props.date}</Text> */}
              <View>
                <Text style={styles.calanderTextContainer}>
                  {this.props.gregorianString}
                </Text>
                <Text style={styles.calanderFarsiTextContainer}>
                  {this.props.jalaaliString}
                </Text>
              </View>
            </View>
          )}
          {this.props.gregorianStart == null ? null : (
            <View
              style={{
                justifyContent: "space-around",
                alignItems: "center",
                flexDirection: "row",
                flex:1
              }}
            >
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Icon
                  name="departure"
                  size={15}
                  style={{ color: "#15479F", paddingRight: 5 }}
                />
                {/* <Text>{this.props.date}</Text> */}
                <View style={{ alignItem: "center", justifyContent: "center" }}>
                  <Text style={styles.calanderTextContainer}>
                    {this.props.gregorianStart}
                  </Text>
                  <Text style={styles.calanderFarsiTextContainer}>
                    {this.props.jamaliStart}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <View style={{ justifyContent: "flex-end" }}>
                  <Text style={styles.calanderTextContainer}>
                    {this.props.gregorianFinish}
                  </Text>
                  <Text style={styles.calanderFarsiTextContainer}>
                    {this.props.jamaliFinish}
                  </Text>
                </View>
                <Icon
                  name="departure"
                  size={15}
                  style={{
                    color: "#15479F",
                    paddingRight: 5,
                    transform: [{ rotateY: "180deg" }]
                  }}
                />
              </View>
            </View>
          )}
        </View>
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  lightCalanderContainer: {
    height: "100%",
    width: "100%",
    alignItems: "center",
    backgroundColor: "#E9EEF6",
    borderRadius: 6,
    marginTop: 10
  },
  calanderContainer: {
    width: "90%",
    height: "100%"
  },
  calanderTextContainer: {
    textAlign: "center",
    fontFamily: "Montserrat",
    color: "#15479F",
    fontSize: RFPercentage(2)
    // alignSelf:'center'
  },
  calanderFarsiTextContainer: {
    textAlign: "center",
    fontFamily: "Sahel",
    color: "#15479F",
    fontSize: RFPercentage(1.7)
  },

  containerHeader: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    position:'absolute',
    // backgroundColor : 'pink',
    top:0,
    zIndex : 5,
    width : '100%'
  },
  containerIcon: {
    color: "#91A9CD"
  },
  containerTitle: {
    fontFamily: "Sahel",
    color: "#91A9CD",
    fontSize: RFPercentage(1.8)
  }
});
