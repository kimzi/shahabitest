import React, { Component } from "react";
import {
  StyleSheet,
  Platform,
  Text,
  View,
  TouchableOpacity
} from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
import { RFPercentage, RFValue }  from "react-native-responsive-fontsize"
const Icon = createIconSetFromFontello(fontelloConfig);

export default class WatermarkIconBlack extends Component {
  render() {
    return <Icon name="group" style={styles.waterMark} size ={100} color='#1B275A'/>;
  }
}

export const styles = StyleSheet.create({
  waterMark: {
    fontSize: RFPercentage(23),
    position: "absolute",
    bottom: 0,
    zIndex: -1,
    opacity :0.15,
  }
});
