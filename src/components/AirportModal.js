import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  PixelRatio,
  TouchableWithoutFeedback,
  Modal,
  TextInput,
  FlatList
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
import { Dimensions } from "react-native";
import Hotel, { styles } from "../pages/MainSearch/Hotel";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
const axios = require("axios");
const Icon = createIconSetFromFontello(fontelloConfig);
export default class AirportModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      airports: ""
    };
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  render() {
    return (
      <View style={styles.container}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "center",
              backgroundColor: "yellow"
            }}
          >
            <View
              style={{
                backgroundColor: "white",
                marginHorizontal: 0,
                padding: 0
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: 0,
                  padding: 20,
                  // height: 50,
                  // backgroundColor: "red",
                  // borderColor: "#15479F",
                  borderWidth: 0
                }}
              >
                <TouchableOpacity
                  onPress={() => this.setModalVisible(!this.state.modalVisible)}
                >
                  <Icon name="cancel" size={15} color="#B5C9E8" />
                </TouchableOpacity>
                <TextInput
                  style={{
                    flex: 1,
                    fontSize: RFPercentage(2.8),
                    // backgroundColor: "yellow",
                    padding: 0,
                    // color: "#B5C9E8",
                    marginLeft: 20
                  }}
                  placeholder="فرودگاه مقصد را مشخص کنید"
                  keyboardType="default"
                  placeholderTextColor="#B5C9E8"
                  autoCapitalize="words"
                  onChangeText={
                    text => {
                      if (text.length > 2) {
                        console.warn(text)
                        axios
                          .post("http://192.168.31.196:1398/api/mobile/Airport/Search", {
                            Airport: text
                          })
                          .then(response => {
                            this.setState({ airports: response.data.Result });
                          })
                          .catch(function(error) {
                            console.warn(error);
                          });
                      }
                    }
                    // text => this.updateValue(text, "PlaceOfBirth")
                  }
                />
              </View>

              <View
                style={{ height: 2, width: "100%", backgroundColor: "#B5C9E8" }}
              />
            </View>

            <View
              style={{
                backgroundColor: "white",
                flex: 1,
                paddingHorizontal: 20,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <FlatList
                data={this.state.airports}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <View
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <Text
                      style={[prestyles.countryText]}
                      numberOfLines={1}
                      ellipsizeMode={"middle"}
                      // onPress={() => this.selectedPassanger(item.index)}
                      onPress={() => console.warn(item.ID)}
                    >
                      {item.Title}
                    </Text>
                    <View
                      style={{
                        height: 1,
                        width: "90%",
                        justifyContent: "center",
                        alignItems: "center",
                        backgroundColor: "#B5C9E8"
                      }}
                    />
                  </View>
                )}
              />
            </View>
          </View>
        </Modal>
        <View style={[prestyles.destinationDepartureContainer]}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.setModalVisible(true);
            }}
          >
            <View
              style={[
                prestyles.destination,
                { marginRight: 5 },
                { alignItems: "center" }
              ]}
            >
              <View style={[styles.containerHeader, { width: "80%" }]}>
                <Icon
                  name="destination"
                  style={styles.containerIcon}
                  color="red"
                />
                <Text style={styles.containerTitle}>مقصد</Text>
              </View>
              <View style={{ flex: 1, justifyContent: "center" }}>
                <View style={[prestyles.textContainer]}>
                  <Text style={prestyles.airportName}>CDG</Text>
                  <Text style={prestyles.cityAirport}>Tehran, Iran</Text>
                  <Text style={prestyles.cityDetails}>
                    IKA Airport + Nearby
                  </Text>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>

          <View
            style={{
              position: "absolute",
              borderWidth: 4,
              borderColor: "#E9EEF6",
              width: "15%",
              height: "25%",
              zIndex: 1,
              elevation: 3,
              shadowColor: "#000",
              shadowOpacity: 0.3,
              shadowRadius: 1,
              shadowOffset: { width: 0, height: 0 },
              backgroundColor: "#fff",
              borderRadius: 20,
              justifyContent: "center",
              alignItems: "center",
              flex: 1
            }}
          >
            <Icon
              name="displacement"
              size={PixelRatio.get() < 2 ? 15 : 20}
              color="#F09819"
            />
          </View>

          <TouchableWithoutFeedback
            onPress={() => {
              this.setModalVisible(true);
            }}
          >
            <View
              style={[
                prestyles.destination,
                { marginLeft: 5 },
                { alignItems: "center" }
              ]}
            >
              <View style={[styles.containerHeader, { width: "80%" }]}>
                <Icon name="departure" style={styles.containerIcon} />
                <Text style={styles.containerTitle}>مبدا</Text>
              </View>
              <View style={{ flex: 1, justifyContent: "center" }}>
                <View style={[prestyles.textContainer]}>
                  <Text style={prestyles.airportName}>CDG</Text>
                  <Text style={prestyles.cityAirport}>Tehran, Iran</Text>
                  <Text style={prestyles.cityDetails}>
                    IKA Airport + Nearby
                  </Text>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }
}

export const prestyles = StyleSheet.create({
    destinationDepartureContainer: {
        height: PixelRatio.get() >= 3 ? "23%" : "30%",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10
      },
      destination: {
        backgroundColor: "#E9EEF6",
        borderRadius: 10,
        flexDirection: "column",
        width: "41%",
        height: "100%"
      },
      textContainer: {
        alignItems: "center",
        flexDirection: "column"
      },
      airportName: {
        color: "#15479F",
        fontFamily: "Montserrat-Bold",
        fontSize: RFPercentage(5.5),
        justifyContent: "center",
        textAlign: "center"
      },
      cityAirport: {
        fontFamily: "Montserrat-Regular",
        fontSize: RFPercentage(1.8),
        color: "#9EADC5"
      },
      cityDetails: {
        fontFamily: "Montserrat-Regular",
        fontSize: RFPercentage(1.6),
        color: "#9EADC5"
      },
      BtnSearch: {
        width: "85%",
        height: PixelRatio.get() >= 3 ? "7%" : "9%",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 30,
        marginTop: 20
      },
  countryText: {
    fontFamily: "Sahel",
    textAlign: "center",
    fontSize: RFPercentage(2.4),
    marginVertical: 5,
    color: "black"
  }
});
